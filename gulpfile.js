'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var typescript = require('gulp-typescript');
var concat = require('gulp-concat');
var strip = require('gulp-strip-comments');

function startExpress() {

  var express = require('express');
  var app = express();
  app.use(express.static(__dirname));
  app.listen(12400);
}

gulp.task('express', function() {
	startExpress();
});

gulp.task('default', ['express', 'scss', 'typescript', 'watch']);
gulp.task('update', ['scss', 'typescript']);
gulp.task('scss', function() {
	return gulp.src('./src/scss/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('./static/css'));
});

gulp.task('typescript', function() {
	return gulp.src(['./src/typescript/*.ts', './src/typescript/*/*.ts', './src/typescript/*/*/*.ts', './src/typescript/*/*/*/*.ts'])
		.pipe(concat('maileditor-library.ts'))
		.pipe(strip())
		.pipe(typescript({
			'module': 'commonjs',
	        'noImplicitAny': true,
	        'removeComments': true,
	        'preserveConstEnums': true,
	        'target': 'ES5',
	        'experimentalDecorators': true,
	        'emitDecoratorMetadata': true
		}))
		.pipe(gulp.dest('./static/js'));
});

gulp.task('watch', function() {
	gulp.watch('./src/typescript/**/*.ts', ['typescript']);
	gulp.watch('./src/scss/**/*.scss', ['scss']);
});
