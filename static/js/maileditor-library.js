var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Application = (function () {
    function Application() {
        this._linked = [];
        this.DOMELEMENT = $el.find('body');
    }
    Application.prototype.addElement = function () {
        var elements = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            elements[_i - 0] = arguments[_i];
        }
        for (var i = elements.length - 1; i >= 0; i--) {
            this.DOMELEMENT.append(elements[i]);
        }
    };
    Application.prototype.removeElement = function () {
        var elements = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            elements[_i - 0] = arguments[_i];
        }
        for (var i = elements.length - 1; i >= 0; i--) {
            elements[i].remove();
        }
    };
    return Application;
}());
var ArrayToHTML = (function () {
    function ArrayToHTML() {
    }
    ArrayToHTML.extendsHTMLwithTranslate = function (translateFunction) {
        ArrayToHTML.translateFunction = translateFunction;
    };
    ArrayToHTML.parse = function () {
        var arr = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            arr[_i - 0] = arguments[_i];
        }
        var html = [];
        for (var i = 0; i < arr.length; i++) {
            var $elm = arr[i];
            var element = new $el($elm[0]);
            if ($elm.length > 1) {
                if ($elm[1] !== null) {
                    var $attributes = $elm[1];
                    for (var x in $attributes) {
                        if ($attributes.hasOwnProperty(x)) {
                            var $attrval = $attributes[x];
                            x = x.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
                            if (x.substr(0, 2) == 'on') {
                                element.on(x.substr(2), $attrval);
                            }
                            else {
                                if (typeof $attrval === "string") {
                                    if (ArrayToHTML.translateFunction) {
                                        $attrval = $attrval.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                                            var translated = ArrayToHTML.translateFunction(content);
                                            if (translated === null) {
                                                return '{t(' + content + ')}';
                                            }
                                            return translated;
                                        });
                                    }
                                }
                                element.attr(x, $attrval);
                            }
                        }
                    }
                }
            }
            if ($elm.length > 2) {
                for (var o = 0; o < $elm.length - 2; o++) {
                    var $cElm = $elm[o + 2];
                    if ($cElm instanceof Array) {
                        $cElm = ArrayToHTML.parse($cElm);
                        for (var p = 0; p < $cElm.length; p++) {
                            element.append($cElm[p]);
                        }
                    }
                    else if (typeof $cElm === "string") {
                        element.append($cElm);
                    }
                }
            }
            html.push(element);
        }
        return html;
    };
    ArrayToHTML.prototype.toString = function (Elm) {
        var html = [];
        for (var i = 0; i < Elm.length; i++) {
            html.push(Elm[i].outerHTML);
        }
        return html.join('');
    };
    ArrayToHTML.translateFunction = null;
    return ArrayToHTML;
}());
var $el = (function (_super) {
    __extends($el, _super);
    function $el(element) {
        _super.call(this);
        this.reload(element);
    }
    $el.prototype.reload = function (element) {
        if (this.length > 0) {
            for (var i = 0; i < this.length; i++) {
                delete this[i];
            }
            this.length = 0;
        }
        if (typeof element === "string") {
            this[this.length] = document.createElement(element);
            this.length++;
        }
        else if (typeof element.length !== "undefined") {
            var elem = [].slice.call(element);
            for (var i = 0; i < elem.length; i++) {
                this[this.length] = elem[i];
                this.length++;
            }
        }
        else {
            this[this.length] = element;
            this.length++;
        }
    };
    $el.prototype.find = function (query) {
        return $el.find(query, this);
    };
    $el.find = function (query, context) {
        var list = [];
        var $contexts;
        if (typeof context === "undefined") {
            $contexts = new $el(document.documentElement);
        }
        else if (context instanceof $el) {
            $contexts = context;
        }
        else {
            $contexts = new $el(context);
        }
        $contexts.each(function () {
            if (query.match(/^\#([a-z0-9-_]+)$/i)) {
                list = list.concat([this.getElementById(query.substr(1))]);
            }
            else if (query.match(/^\.([a-z0-9-_]+)$/i)) {
                list = list.concat([].slice.call(this.getElementsByClassName(query.substr(1))));
            }
            else if (query.match(/^([a-z0-9-_]+)$/i)) {
                list = list.concat([].slice.call(this.getElementsByTagName(query)));
            }
            else {
                list = list.concat([].slice.call(this.querySelectorAll(query)));
            }
        });
        return new $el(list);
    };
    $el.fromHTML = function (html) {
        var elem = document.createElement('template');
        elem.innerHTML = html;
        return new $el(elem.childNodes);
    };
    $el.prototype.get = function (index) {
        return this[index];
    };
    $el.prototype.first = function () {
        return this[0];
    };
    $el.prototype.last = function () {
        return this[this.length - 1];
    };
    $el.prototype.addClass = function (className) {
        this.each(function () {
            if (typeof document.documentElement.classList !== "undefined") {
                this.classList.add(className);
            }
            else {
                if (!this._hasClass(className, this)) {
                    this.className += ' ' + className;
                }
            }
        });
        return this;
    };
    $el.prototype.removeClass = function (className) {
        this.each(function () {
            if (typeof document.documentElement.classList !== "undefined") {
                this.classList.remove(className);
            }
            else {
                if (!this._hasClass(className, this)) {
                    this.className = this.className.replace(new RegExp('\\b' + className + '\\b', 'g'), '');
                }
            }
        });
        return this;
    };
    $el.prototype.hasClass = function (className) {
        return this._hasClass(className, this.first());
    };
    $el.prototype._hasClass = function (className, element) {
        return typeof document.documentElement.classList !== "undefined" ?
            element.classList.contains(className) :
            new RegExp('\\b' + className + '\\b').test(element.className);
    };
    $el.prototype.matches = function (selector) {
        return this._matches(this.first(), selector);
    };
    $el.prototype._matches = function (el, selector) {
        var p = Element.prototype;
        var f = p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || function (s) {
            return this._indexOf(el, document.querySelectorAll(s).length > 0 ? [].concat(document.querySelectorAll(s)) : []) !== -1;
        };
        return f.call(el, selector);
    };
    $el.prototype.parent = function () {
        var parents = [];
        this.each(function () {
            parents.push(this.parentNode);
        });
        return new $el(parents);
    };
    $el.prototype.parents = function (selector) {
        var parents = [];
        var _this = this;
        this.each(function () {
            var p = this;
            while (p.parentNode && p.parentNode != document.documentElement) {
                if (typeof selector !== "undefined") {
                    if (_this._matches(p.parentNode, selector)) {
                        parents.push(p.parentNode);
                    }
                }
                else {
                    parents.push(p.parentNode);
                }
                p = p.parentNode;
            }
        });
        return new $el(parents);
    };
    $el.prototype.children = function (selector) {
        var children = [];
        var _this = this;
        this.each(function () {
            if (this.hasChildNodes()) {
                if (typeof selector === "undefined") {
                    children = children.concat([].slice.call(this.childNodes));
                }
                else {
                    for (var i = 0; i < this.childNodes.length; i++) {
                        if (_this._matches(this.childNodes[i], selector)) {
                            children.push(this.childNodes[i]);
                        }
                    }
                }
            }
        });
        return new $el(children);
    };
    $el.prototype.getAttribute = function (attr) {
        return this.first().getAttribute(attr);
    };
    $el.prototype.setAttribute = function (attr, val) {
        this.each(function () {
            this.setAttribute(attr, val);
        });
        return this;
    };
    $el.prototype.removeAttribute = function (attr) {
        this.each(function () {
            this.removeAttribute(attr);
        });
        return this;
    };
    $el.prototype.before = function (element) {
        if (typeof element === 'string') {
            this.each(function () {
                if ($el.translateFunction) {
                    element = element.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                        var translated = $el.translateFunction(content);
                        if (translated === null) {
                            return '{t(' + content + ')}';
                        }
                        return translated;
                    });
                }
                this.insertAdjacentHTML('beforeBegin', element);
            });
        }
        else if (element instanceof $el) {
            this.each(function () {
                var _el = this;
                var newList = [];
                element.each(function () {
                    var child = _el.parentNode.insertBefore(this, _el);
                    newList.push(child);
                });
                element.reload(newList);
            });
        }
        else {
            this.each(function () {
                this.parentNode.insertBefore(element, this);
            });
        }
        return this;
    };
    $el.prototype.after = function (element) {
        var _this = this;
        if (typeof element === 'string') {
            if ($el.translateFunction) {
                element = element.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                    var translated = $el.translateFunction(content);
                    if (translated === null) {
                        return '{t(' + content + ')}';
                    }
                    return translated;
                });
            }
            this.each(function () {
                this.insertAdjacentHTML('afterEnd', element);
            });
        }
        else if (element instanceof $el) {
            this.each(function () {
                var _el = this;
                var newList = [];
                element.each(function () {
                    if (_el.parentNode.lastChild == _el) {
                        var child = _el.parentNode.appendChild(this);
                    }
                    else {
                        var child = _el.parentNode.insertBefore(this, _el.nextSibling);
                    }
                    newList.push(child);
                });
                element.reload(newList);
            });
        }
        else {
            this.each(function () {
                if (this.parentNode.lastChild == this) {
                    this.parentNode.appendChild(element);
                }
                else {
                    this.parentNode.insertBefore(element, this.nextSibling);
                }
            });
        }
        return this;
    };
    $el.prototype.afterArray = function (element) {
        var _this = this;
        $el.each(element, function (i, e) {
            _this.after(e);
        });
        return this;
    };
    $el.prototype.append = function (element) {
        if (typeof element === 'string') {
            if ($el.translateFunction) {
                element = element.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                    var translated = $el.translateFunction(content);
                    if (translated === null) {
                        return '{t(' + content + ')}';
                    }
                    return translated;
                });
            }
            this.each(function () {
                this.insertAdjacentHTML('beforeEnd', element);
            });
        }
        else if (element instanceof $el) {
            this.each(function () {
                var _el = this;
                var newList = [];
                element.each(function () {
                    var child = _el.appendChild(this);
                    newList.push(child);
                });
                element.reload(newList);
            });
        }
        else {
            this.each(function () {
                this.appendChild(element);
            });
        }
        return this;
    };
    $el.prototype.appendArray = function (element) {
        var _this = this;
        $el.each(element, function (i, e) {
            _this.append(e);
        });
        return this;
    };
    $el.prototype.prepend = function (element) {
        if (element) {
            if (typeof element === 'string') {
                if ($el.translateFunction) {
                    element = element.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                        var translated = $el.translateFunction(content);
                        if (translated === null) {
                            return '{t(' + content + ')}';
                        }
                        return translated;
                    });
                }
                this.each(function () {
                    this.insertAdjacentHTML('afterBegin', element);
                });
            }
            else if (element instanceof $el) {
                this.each(function () {
                    var _el = this;
                    var first = this.childNodes[0];
                    var newList = [];
                    element.each(function () {
                        var child = _el.insertBefore(this, first);
                        newList.push(child);
                    });
                    element.reload(newList);
                });
            }
            else {
                this.each(function () {
                    var first = this.childNodes[0];
                    this.insertBefore(element, first);
                });
            }
        }
        return this;
    };
    $el.prototype.clone = function (withEvents, deep) {
        if (withEvents === void 0) { withEvents = true; }
        if (deep === void 0) { deep = true; }
        var newList = [];
        this.each(function () {
            var node = this.cloneNode(deep);
            if (withEvents) {
                for (var event in this._events) {
                    if (this._events.hasOwnProperty(event)) {
                        var _event = this._events[event];
                        for (var i = 0; i < _event.length; i++) {
                            if (typeof document.addEventListener !== "undefined") {
                                this.addEventListener(event, _event[i]);
                            }
                            else {
                                this.attachEvent('on' + event, _event[i]);
                            }
                        }
                    }
                }
            }
            node._events = this._events;
            newList.push(node);
        });
        return new $el(newList);
    };
    $el.prototype.css = function (css, value) {
        if (typeof css === "string" && typeof value === "undefined") {
            if (typeof getComputedStyle !== 'undefined') {
                return getComputedStyle(this.first(), null).getPropertyValue(css);
            }
            else {
                return this.first().currentStyle[css];
            }
        }
        else if (typeof css === "string" && typeof value !== "undefined") {
            this.each(function () {
                this.style[css] = value;
            });
        }
        else {
            this.each(function () {
                for (var k in css) {
                    if (css.hasOwnProperty(k)) {
                        this.style[k] = css[k];
                    }
                }
            });
        }
        return this;
    };
    $el.prototype.attr = function (attr, val) {
        if (typeof val === "undefined") {
            if (typeof attr === "string") {
                return this.getAttribute(attr);
            }
            else {
                var _this = this;
                $el.each(attr, function (key, val) {
                    if (typeof val === null) {
                        _this.removeAttribute(key);
                    }
                    else {
                        if ($el.translateFunction) {
                            if (typeof val === 'string') {
                                val = val.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                                    var translated = $el.translateFunction(content);
                                    if (translated === null) {
                                        return '{t(' + content + ')}';
                                    }
                                    return translated;
                                });
                            }
                        }
                        _this.setAttribute(key, val);
                    }
                });
            }
        }
        else {
            if (val === null) {
                this.removeAttribute(attr);
            }
            else {
                if ($el.translateFunction) {
                    if (typeof val === 'string') {
                        val = val.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                            var translated = $el.translateFunction(content);
                            if (translated === null) {
                                return '{t(' + content + ')}';
                            }
                            return translated;
                        });
                    }
                }
                this.setAttribute(attr, val);
            }
        }
        return this;
    };
    $el.prototype.childNodes = function (value) {
        this.html('');
        for (var i = 0; i < value.length; i++) {
            this.appendArray(value[i]);
        }
        return this;
    };
    $el.prototype.html = function (html) {
        if (typeof html !== "undefined") {
            if ($el.translateFunction) {
                html = html.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                    var translated = $el.translateFunction(content);
                    if (translated === null) {
                        return '{t(' + content + ')}';
                    }
                    return translated;
                });
            }
            this.each(function () {
                this.innerHTML = html;
            });
            return this;
        }
        else {
            return this.first().innerHTML;
        }
    };
    $el.prototype.val = function (val) {
        if (typeof val !== "undefined") {
            this.each(function () {
                this.value = val;
            });
            return this;
        }
        else {
            return this.first().value;
        }
    };
    $el.prototype.text = function (val) {
        if (typeof val !== "undefined") {
            this.each(function () {
                this.innerText = val;
            });
            return this;
        }
        else {
            return (this.first()).innerText;
        }
    };
    $el.prototype.remove = function () {
        this.revEach(function () {
            if (typeof this.parentNode !== "undefined") {
                this.parentNode.removeChild(this);
            }
        });
    };
    $el.prototype.outerHTML = function (html) {
        if (typeof html !== "undefined") {
            if ($el.translateFunction) {
                html = html.replace(/{t\((.*?)\)}/g, function (match, content, offset, string) {
                    var translated = $el.translateFunction(content);
                    if (translated === null) {
                        return '{t(' + content + ')}';
                    }
                    return translated;
                });
            }
            this.each(function () {
                this.outerHTML = html;
            });
            return this;
        }
        else {
            return this.first().outerHTML;
        }
    };
    Object.defineProperty($el.prototype, "width", {
        get: function () {
            return this.first().getBoundingClientRect().width;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty($el.prototype, "height", {
        get: function () {
            return this.first().getBoundingClientRect().height;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty($el.prototype, "innerWidth", {
        get: function () {
            return this.first().clientWidth;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty($el.prototype, "innerHeight", {
        get: function () {
            return this.first().clientHeight;
        },
        enumerable: true,
        configurable: true
    });
    $el.prototype.data = function (attr, val) {
        if (typeof val === "undefined") {
            if (typeof attr === "string") {
                return this.getAttribute('data-' + attr);
            }
            else {
                var _this = this;
                $el.each(attr, function (key, val) {
                    if (typeof val === null) {
                        _this.removeAttribute('data-' + key);
                    }
                    else {
                        _this.setAttribute('data-' + key, val);
                    }
                });
            }
        }
        else {
            if (val === null) {
                this.removeAttribute('data-' + attr);
            }
            else {
                this.setAttribute('data-' + attr, val);
            }
        }
        return this;
    };
    $el.prototype.prop = function (prop, val) {
        if (typeof val === "undefined") {
            if (typeof prop === "string") {
                var first = this.first();
                return first[prop];
            }
            else {
                var _this = this;
                $el.each(prop, function (key, val) {
                    this.each(function () {
                        this[key] = val;
                    });
                });
            }
        }
        else {
            this.each(function () {
                this[prop] = val;
            });
        }
        return this;
    };
    $el.prototype.trigger = function (event) {
        this.each(function () {
            var _this = this;
            if (typeof this._events !== "undefined" && typeof this._events[event] !== "undefined") {
                for (var e = 0; e < this._events[event].length; e++) {
                    this._events[event][e].apply(_this, []);
                }
            }
        });
        return this;
    };
    $el.prototype.on = function (event, listener) {
        this.each(function () {
            if (typeof this._events === "undefined") {
                this._events = {};
            }
            if (typeof this._events[event] === "undefined") {
                this._events[event] = [];
            }
            if (typeof listener === "function") {
                if (typeof document.addEventListener !== "undefined") {
                    this.addEventListener(event, listener);
                }
                else {
                    this.attachEvent('on' + event, listener);
                }
                this._events[event].push(listener);
            }
            else {
                var _this = this;
                $el.each(listener, function (i, ev) {
                    if (typeof document.addEventListener !== "undefined") {
                        _this.addEventListener(event, ev);
                    }
                    else {
                        _this.attachEvent('on' + event, ev);
                    }
                    _this._events[event].push(ev);
                });
            }
        });
        return this;
    };
    $el.prototype.events = function (event) {
        var _events = [];
        this.each(function () {
            if (this._events[event]) {
                $el.each(this._events[event], function (i, event) {
                    _events.push(event);
                });
            }
        });
        return _events;
    };
    $el.prototype.off = function (event, listener) {
        this.each(function () {
            if (typeof this._events !== "undefined" && typeof this._events[event] !== "undefined") {
                if (typeof listener !== "undefined") {
                    if (typeof document.removeEventListener !== "undefined") {
                        this.removeEventListener(event, listener);
                    }
                    else {
                        this.detachEvent('on' + event, listener);
                    }
                    this._events[event].splice(this._events[event].indexOf(listener), 1);
                }
                else {
                    for (var i = 0; i < this._events[event].length; i++) {
                        var listener = this._events[event][i];
                        if (typeof document.removeEventListener !== "undefined") {
                            this.removeEventListener(event, listener);
                        }
                        else {
                            this.detachEvent('on' + event, listener);
                        }
                    }
                    this._events[event] = [];
                }
            }
        });
        return this;
    };
    $el.indexOf = function (needle, haystack) {
        if (typeof haystack.indexOf !== "function") {
            for (var i = 0; i < haystack.length; i++) {
                if (haystack[i] === needle) {
                    return i;
                }
            }
            return -1;
        }
        else {
            return haystack.indexOf(needle);
        }
    };
    $el.prototype.each = function (func) {
        $el.each(this, func);
        return this;
    };
    $el.prototype.revEach = function (func) {
        $el.revEach(this, func);
        return this;
    };
    $el.each = function (arr, func) {
        if (arr.length) {
            for (var i = 0; i < arr.length; i++) {
                func.apply(arr[i], [i, arr[i]]);
            }
        }
        else {
            for (var name in arr) {
                if (arr.hasOwnProperty(name)) {
                    func.apply(arr[name], [name, arr[name]]);
                }
            }
        }
    };
    $el.revEach = function (arr, func) {
        for (var i = arr.length - 1; i >= 0; i--) {
            func.apply(arr[i], [i, arr[i]]);
        }
    };
    $el.from = function (item) {
        return new $el(item);
    };
    $el.extendsHTMLwithTranslate = function (translateFunction) {
        $el.translateFunction = translateFunction;
    };
    $el.ready = function (func) {
        if (document.readyState != 'loading')
            func();
        else if (document.addEventListener)
            document.addEventListener('DOMContentLoaded', func);
        else
            document.attachEvent('onreadystatechange', function () { if (document.readyState == 'complete')
                func(); });
    };
    $el.trim = function (string) {
        if (typeof string.trim !== "undefined") {
            return string.trim();
        }
        else {
            return string.replace(/^\s+|\s+$/g, '');
        }
    };
    $el.jsonParse = function (json) {
        if (typeof JSON !== "undefined") {
            return JSON.parse(json);
        }
        else {
            return eval(json);
        }
    };
    $el.get = function (url, func) {
        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        xhr.open('GET', url, false);
        xhr.onreadystatechange = function () {
            if (xhr.readyState > 3 && xhr.status == 200)
                func(xhr.responseText);
        };
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send();
        return xhr;
    };
    $el.translateFunction = null;
    return $el;
}(Array));
var Application;
(function (Application) {
    var Dialog = (function () {
        function Dialog(html) {
            this.DOMELEMENT = new $el('dialogwindow');
            var dialog = new $el('div').addClass('dialog');
            var dialogbg = new $el('div').addClass('dialog-bg');
            dialog.childNodes(ArrayToHTML.parse(html));
            this.DOMELEMENT.append(dialogbg).append(dialog);
            Application.Dialog.document.append(this.DOMELEMENT);
        }
        Dialog.prototype.close = function () {
            this.DOMELEMENT.remove();
        };
        return Dialog;
    }());
    Application.Dialog = Dialog;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Draggable = (function () {
        function Draggable(view, context, hover) {
            var body = $el.find('body');
            var _this = this;
            this.DOMELEMENT = new $el('draggable');
            if (typeof view === "string") {
                this.DOMELEMENT.html(view);
            }
            else {
                this.DOMELEMENT.html((view.outerHTML()));
            }
            $el.find('draggable').remove();
            this.DOMELEMENT.css({ 'position': 'absolute', 'top': '-1000px', 'left': '-1000px' });
            this._hover = hover;
            body.on('mousemove', function (e) {
                _this.DOMELEMENT.css({ 'top': (e.clientY + 18) + 'px', 'left': (e.clientX + 18) + 'px' });
            });
            this._context = context;
            this.hoverElements = $el.find(hover.allowed.join(','));
            this.hoverElements.off('mouseleave').off('mouseenter').on('mouseleave', function () {
                $el.from(this).removeClass('hover');
            }).on('mouseenter', function () {
                $el.from(this).addClass('hover');
            });
            this._context.off('mouseup').on('mouseup', function () {
                $el.find('draggable').remove();
                _this._context.off('mouseup');
            });
            body.append(this.DOMELEMENT);
        }
        Object.defineProperty(Draggable.prototype, "release", {
            set: function (release) {
                var _this = this;
                this._context.off('mouseup').on('mouseup', function (e) {
                    _this.hoverElements.removeClass('hover').off('mouseleave').off('mouseenter');
                    _this._context.off('mouseup');
                    $el.find('body').off('mousemove');
                    release(e, _this.DOMELEMENT);
                    _this.DOMELEMENT.remove();
                });
            },
            enumerable: true,
            configurable: true
        });
        ;
        Draggable.prototype.remove = function () {
            this.hoverElements.removeClass('hover').off('mouseleave').off('mouseenter');
            this._context.off('mouseup');
            $el.find('body').off('mousemove');
            this.DOMELEMENT.remove();
        };
        return Draggable;
    }());
    Application.Draggable = Draggable;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Editor = (function () {
        function Editor() {
            this._watch = {};
            this._selfAttached = undefined;
            this.DOMELEMENT = new $el('editor');
            this.SELECTED_DOMELEMENT = this.DOMELEMENT.clone();
            this.DOMELEMENT.childNodes(ArrayToHTML.parse([
                'table', { cellspacing: 0, cellpadding: 0, border: 0, style: 'width: 600px' },
                ['tbody', null,
                    ['tr', null,
                        ['td', { class: 'placeholder' },
                            'This is a placeholder.'
                        ]
                    ]
                ]
            ]));
        }
        Editor.prototype.triggerWatch = function (_variable) {
            for (var nm in this._watch[_variable]) {
                if (this._watch[_variable].hasOwnProperty(nm)) {
                    this._watch[_variable][nm].apply(this, []);
                }
            }
        };
        Editor.prototype.joinWatch = function (_variable, nm, func) {
            if (typeof this._watch[_variable] === "undefined") {
                this._watch[_variable] = {};
            }
            this._watch[_variable][nm] = function () { func.apply(this, [this[_variable]]); };
        };
        Editor.prototype.reattachEvents = function () {
            var _this = this;
            var elements = this.DOMELEMENT.find('td.content');
            if (this._selfAttached) {
                elements.off('click', this._selfAttached);
                this.DOMELEMENT.off('click', this._selfAttached);
            }
            this._selfAttached = function (e) {
                _this.SELECTED_DOMELEMENT = $el.from(this);
                _this.triggerWatch('SELECTED_DOMELEMENT');
                e.stopPropagation();
            };
            elements.on('click', this._selfAttached);
            this.DOMELEMENT.on('click', this._selfAttached);

            this.DOMELEMENT.find('div[contenteditable]').each(function() {
                this.onpaste = function(e) {
                    e.preventDefault();
                    /*var text = (e.originalEvent || e).clipboardData.getData('text/html');

                     text = (text).replace(/<(?!\/?(p|br|ul|li|b|i|sub|sup|strike|em|strong|span|table|caption|tbody|tr|td)(>|\s))[^<]+?>/g, '');
                     // remove font-family, font-size, line-height from style
                     text = (text).replace(/font-family\:[^;]+;?|font-size\:[^;]+;?|line-height\:[^;]+;?/g, '');
                     // remove if style is empty, e.g. style="" || style="   "
                     text = (text).replace(/style\="\s*?"/, '');
                     // remove empty tags, e.g. <p></p>
                     text = (text).replace(/<[^\/>][^>]*><\/[^>]+>/, '');
                     // remove ids and classes
                     text = (text).replace(/\s(id|class)="[^"]+"/gi, '');

                     //remove all styles from tags
                     //evt.data['html'] = (evt.data['html']).replace(/(<[^>]+) style=".*?"/i, '');
                     */
                    var text = (e.originalEvent || e).clipboardData.getData('text/plain');
                    window.document.execCommand('insertHTML', false, text);
                };});
        };
        Editor.prototype.addElement = function () {
            var elements = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                elements[_i - 0] = arguments[_i];
            }
            for (var i = elements.length - 1; i >= 0; i--) {
                this.DOMELEMENT.append(elements[i]);
            }
        };
        Editor.prototype.removeElement = function () {
            var elements = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                elements[_i - 0] = arguments[_i];
            }
            for (var i = elements.length - 1; i >= 0; i--) {
                elements[i].remove();
            }
        };
        return Editor;
    }());
    Application.Editor = Editor;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var FileManager = (function () {
        function FileManager() {
            this._files = [];
            this._onNewFile = function () {
            };
            this._onRemFile = function () {
            };
            this.DOMELEMENT = new $el('filemanager');
            this.DOMELEMENT.childNodes(ArrayToHTML.parse(['section', { class: 'title' }, '{t(files)}'], ['div', { class: 'files' }], ['div', { class: 'input' },
                ['input', { type: 'file', multiple: 'multiple' }],
                ['div', { class: 'upload' },
                    ['i', { class: 'fa fa-cloud-upload fa-fw' }],
                    ' {t(upload.file)}'
                ]
            ]));
            var _this = this;
            this.DOMELEMENT.find('.upload').on('click', function (e) {
                _this.DOMELEMENT.find('input').first().click();
            });
            this.DOMELEMENT.find('input').on('change', function (e) {
                $el.each(e.target.files, function (i, file) {
                    if (file.type.substr(0, 5) == 'image') {
                        var reader = new FileReader();
                        reader.addEventListener("load", function () {
                            var $file = new Application.FileManager.Image(function () { _this._onRemFile.apply(_this, arguments); });
                            $file.setFileData(file, reader.result);
                            _this._files.push($file.getFileData());
                            _this._onNewFile($file.getFileData());
                            _this.addElement($file);
                        }, false);
                        reader.readAsDataURL(file);
                    }
                    else {
                        var reader = new FileReader();
                        reader.addEventListener("load", function () {
                            var $file = new Application.FileManager.File(function () { _this._onRemFile.apply(_this, arguments); });
                            $file.setFileData(file, reader.result);
                            _this._files.push($file.getFileData());
                            _this._onNewFile($file.getFileData());
                            _this.addElement($file);
                        }, false);
                        reader.readAsDataURL(file);
                    }
                });
                $el.from(this).val('');
            });
        }
        FileManager.prototype.onNewFile = function (func) {
            this._onNewFile = func;
        };
        FileManager.prototype.onRemFile = function (func) {
            this._onRemFile = func;
        };
        FileManager.prototype.getFiles = function () {
            return this._files;
        };
        FileManager.prototype.loadFiles = function (files) {
            var _this = this;
            this.DOMELEMENT.find('.files').html('');
            for (var a = 0; a < files.length; a++) {
                var file = files[a];
                var $file = null;
                if (file.type.substr(0, 5) === 'image') {
                    $file = new Application.FileManager.Image(function () { _this._onRemFile.apply(_this, arguments); });
                    $file.setFileData(file, file.src);
                    this._files.push(file.src);
                    this.addElement($file);
                }
                else {
                    $file = new Application.FileManager.File(function () { _this._onRemFile.apply(_this, arguments); });
                    $file.setFileData(file, file.src);
                    this._files.push($file.getFileData());
                    this.addElement($file);
                }
            }
        };
        FileManager.prototype.addElement = function (element) {
            this.DOMELEMENT.find('.files').append(element.DOMELEMENT);
            return this;
        };
        return FileManager;
    }());
    Application.FileManager = FileManager;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Language = (function () {
        function Language() {
        }
        Language.register = function (lang, langConf) {
            Language.lang = langConf;
            Language.language = lang;
        };
        Language.translate = function (word) {
            if (typeof Language.lang[word] !== "undefined") {
                return Language.lang[word];
            }
            else if (word.indexOf('.') !== -1) {
                var wordList = word.split('.');
                var ln = Language.lang;
                for (var wordList_i = 0; wordList_i < wordList.length; wordList_i++) {
                    var subWordList = wordList.slice(wordList_i, wordList.length);
                    for (var subWordList_i = subWordList.length; subWordList_i > 0; subWordList_i--) {
                        var subWord = subWordList.slice(0, subWordList_i).join('.');
                        if (typeof ln[subWord] !== "undefined") {
                            ln = ln[subWord];
                            wordList_i += subWordList_i - 1;
                            break;
                        }
                    }
                }
                if (ln) {
                    return ln;
                }
            }
            return null;
        };
        Language.lang = {};
        Language.language = '';
        return Language;
    }());
    Application.Language = Language;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Popup = (function () {
        function Popup(html) {
            this.DOMELEMENT = new $el('popup');
            var popup = new $el('div').addClass('popup');
            var popupbg = new $el('div').addClass('popup-bg');
            popup.childNodes(ArrayToHTML.parse(html));
            this.DOMELEMENT.append(popupbg).append(popup);
            Application.Popup.document.append(this.DOMELEMENT);
        }
        Popup.prototype.close = function () {
            this.DOMELEMENT.remove();
        };
        return Popup;
    }());
    Application.Popup = Popup;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Properties = (function () {
        function Properties(editor, properties) {
            this.DOMELEMENT = new $el('properties');
            this.DOMELEMENT.childNodes(this.propertiesToHTML(editor, properties));
            var _this = this;
            $el.each(properties, function (prop, property) {
                var $input = _this.DOMELEMENT.find('input[name="' + prop + '"]');
                property.initialize($input.get(0));
                editor.joinWatch('SELECTED_DOMELEMENT', 'bar-prop-' + prop, function () {
                    property.getValue(editor.SELECTED_DOMELEMENT, $input);
                });
                $input.on('focus', function () {
                    $el.from(document.body).trigger("click");
                });
                $input.on('change', function () {
                    property.setValue(editor.SELECTED_DOMELEMENT, $input);
                });
                $input.on('blur', function () {
                    property.setValue(editor.SELECTED_DOMELEMENT, $input);
                });
            });
        }
        Properties.prototype.addElement = function (element) {
            this.DOMELEMENT.find('.files').append(element.DOMELEMENT);
            return this;
        };
        Properties.prototype.propertiesToHTML = function (editor, properties) {
            var html = [];
            for (var prop in properties) {
                if (properties.hasOwnProperty(prop)) {
                    var val = properties[prop];
                    var htmlString = ['li', { class: 'field' },
                        ['label', null, '{t(properties.' + prop + ')}'],
                        ['input', { type: 'text', name: prop.toLowerCase(), value: val.default }]
                    ];
                    html.push(htmlString);
                }
            }
            return ArrayToHTML.parse(['ul', null].concat(html));
        };
        return Properties;
    }());
    Application.Properties = Properties;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Toolbar = (function () {
        function Toolbar() {
            this.DOMELEMENT = new $el('toolbar');
        }
        Toolbar.prototype.loadConfig = function (editor, config) {
            var _this = this;
            $el.each(config, function (group_index, group) {
                if (group_index > 0) {
                    var seperator = new Toolbar.Seperator();
                    _this.DOMELEMENT.append(seperator.DOMELEMENT);
                }
                $el.each(group, function (button_index, button) {
                    var btn = new Toolbar.Button(editor);
                    var htm = button.html;
                    btn.html(htm);
                    if (button.load) {
                        button.load(btn.DOMELEMENT);
                    }
                    editor.joinWatch('SELECTED_DOMELEMENT', (group_index + '-' + button_index), function () {
                        if (button.contentCheck) {
                            button.contentCheck(btn.DOMELEMENT, editor.SELECTED_DOMELEMENT);
                        }
                    });
                    btn.setAction(button, button.action);
                    _this.DOMELEMENT.append(btn.DOMELEMENT);
                });
            });
        };
        Toolbar.prototype.addElement = function () {
            var elements = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                elements[_i - 0] = arguments[_i];
            }
            for (var i = elements.length - 1; i >= 0; i--) {
                this.DOMELEMENT.append(elements[i]);
            }
        };
        Toolbar.prototype.removeElement = function () {
            var elements = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                elements[_i - 0] = arguments[_i];
            }
            for (var i = elements.length - 1; i >= 0; i--) {
                elements[i].remove();
            }
        };
        return Toolbar;
    }());
    Application.Toolbar = Toolbar;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Toolkit = (function () {
        function Toolkit() {
            this.DOMELEMENT = new $el('toolkit');
        }
        Toolkit.prototype.loadConfig = function (editor, config) {
            var _this = this;
            $el.each(config, function (group_index, group) {
                if (group_index > 0) {
                    var seperator = new Toolkit.Seperator();
                    _this.DOMELEMENT.append(seperator.DOMELEMENT);
                }
                $el.each(group, function (button_index, button) {
                    var btn = new Toolkit.Button();
                    btn.setSources(button.icons[0]);
                    btn.title = button.title;
                    btn.setAction(button, button.action, editor);
                    _this.DOMELEMENT.append(btn.DOMELEMENT);
                });
            });
        };
        Toolkit.prototype.addElement = function () {
            var elements = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                elements[_i - 0] = arguments[_i];
            }
            for (var i = elements.length - 1; i >= 0; i--) {
                this.DOMELEMENT.append(elements[i]);
            }
        };
        Toolkit.prototype.removeElement = function () {
            var elements = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                elements[_i - 0] = arguments[_i];
            }
            for (var i = elements.length - 1; i >= 0; i--) {
                elements[i].remove();
            }
        };
        return Toolkit;
    }());
    Application.Toolkit = Toolkit;
})(Application || (Application = {}));
var Application;
(function (Application) {
    var FileManager;
    (function (FileManager) {
        var File = (function () {
            function File(_rmAction) {
                this._removeAction = function () { };
                var _this = this;
                this._removeAction = _rmAction;
                this.DOMELEMENT = new $el('div');
                this.DOMELEMENT.addClass('file');
                this.DOMELEMENT.childNodes(ArrayToHTML.parse(['div', { class: 'file-icon', 'data-filetype': 'new' },
                    ['div', null, 'new']
                ], ['div', { class: 'file-info' },
                    ['div', { dataName: 'name' }],
                    ['div', { dataName: 'size' }]
                ], ['div', { class: 'file-actions' },
                    '<i class="fa fa-trash-o fa-fw remove-file"></i>'
                ]));
                this.DOMELEMENT.find('.remove-file').on('mousedown', function (e) {
                    e.stopPropagation();
                });
                this.DOMELEMENT.find('.remove-file').on('click', function (e) {
                    var popup = new Application.Popup([
                        'div', { class: 'content' },
                        ['div', { class: 'title' }, '{t(remove.file.message)}'],
                        ['div', { class: 'buttons' },
                            ['span', {
                                    class: 'button bad',
                                    'data-name': 'remove',
                                    onclick: function () {
                                        _this.DOMELEMENT.remove();
                                        popup.close();
                                        _this._removeAction(_this._fileData);
                                    }
                                }, '{t(popup.remove)}'],
                            ['span', { class: 'button', onclick: function () { popup.close(); } }, '{t(popup.cancel)}']
                        ]
                    ]);
                });
            }
            File.prototype.setFileData = function (fileData, content) {
                var ext = fileData.name.substr(fileData.name.lastIndexOf('.') + 1).toLowerCase();
                this.DOMELEMENT.find('[data-name="name"]').html(fileData.name);
                this.DOMELEMENT.find('[data-name="size"]').html(this.formatFileSize(fileData.size));
                this.DOMELEMENT.find('.file-icon').attr('data-filetype', ext);
                this.DOMELEMENT.find('.file-icon > div').html(ext);
                fileData.content = content;
                fileData.filename = fileData.name;
                this._fileData = fileData;
            };
            File.prototype.formatFileSize = function (filesize) {
                if (filesize === 0) {
                    return '0 B';
                }
                else {
                    var index = Math.floor(Math.log(filesize) / Math.log(1024));
                    var fsz = filesize / Math.pow(1024, index);
                    return fsz.toFixed(2) + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][index];
                }
            };
            File.prototype.getFileData = function () {
                return this._fileData;
            };
            return File;
        }());
        FileManager.File = File;
    })(FileManager = Application.FileManager || (Application.FileManager = {}));
})(Application || (Application = {}));
var Application;
(function (Application) {
    var FileManager;
    (function (FileManager) {
        var Image = (function () {
            function Image(_rmAction, fileData) {
                if (fileData === void 0) { fileData = null; }
                this._removeAction = function () { };
                var _this = this;
                this._removeAction = _rmAction;
                this.DOMELEMENT = new $el('div');
                this.DOMELEMENT.addClass('file');
                this.DOMELEMENT.childNodes(ArrayToHTML.parse(['div', { class: 'image' },
                    ['img', { dataName: 'preview' }]
                ], ['div', { class: 'file-info' },
                    ['div', { dataName: 'name' }],
                    ['div', { dataName: 'size' }]
                ], ['div', { class: 'file-actions' },
                    '<i class="fa fa-trash-o fa-fw remove-file"></i>'
                ]));
                this.DOMELEMENT.find('.remove-file').on('mousedown', function (e) {
                    e.stopPropagation();
                });
                var moveable = null;
                this.DOMELEMENT.find('.remove-file').on('click', function (e) {
                    if (moveable !== null) {
                        moveable.remove();
                    }
                    var popup = new Application.Popup([
                        'div', { class: 'content' },
                        ['div', { class: 'title' }, '{t(remove.file.message)}'],
                        ['div', { class: 'buttons' },
                            ['span', {
                                    class: 'button bad',
                                    'data-name': 'remove',
                                    onclick: function () {
                                        $el.find('editor').find('[data-slot="image"]').each(function (i, __this) {
                                            var $this = $el.from(__this);
                                            if ($this.find('img').matches('[data-src="' + _this._fileData.filename + '"]') || $this.find('img').matches('[data-src="' + _this._fileData.src + '"]')) {
                                                $this.attr('data-slot', null);
                                                $this.html('');
                                                Application.FileManager.Image.resetDataSlot($this);
                                            }
                                        });
                                        _this.DOMELEMENT.remove();
                                        popup.close();
                                        _this._removeAction(_this._fileData);
                                    }
                                }, '{t(popup.remove)}'],
                            ['span', { class: 'button', onclick: function () { popup.close(); } }, '{t(popup.cancel)}']
                        ]
                    ]);
                });
                var hover = { allowed: ['.slot'] };
                this.DOMELEMENT.on('mousedown', function () {
                    var $img = $el.from(this).find('img').clone();
                    $img.attr('data-name', null);
                    $img.css('display', 'block');
                    var _img = $img;
                    moveable = new Application.Draggable($img, Application.Draggable.Context, hover);
                    moveable.release = function (e, $img) {
                        var $target = $el.from(e.target);
                        if ($target.parents('.slot').length == 1) {
                            $target = $target.parents('.slot');
                        }
                        if (!$target.matches('.slot')) {
                            $target.off('click');
                            moveable = null;
                            return;
                        }
                        var $remove = new $el('div');
                        $remove.addClass('remove');
                        var targetOldEvents = $target.events('click');
                        $remove.on('click', function (e) {
                            e.stopPropagation();
                            var p = $remove.parent().parent();
                            Application.FileManager.Image.resetDataSlot($remove.parent());
                            $remove.parent().on('click', targetOldEvents);
                            $remove.parent().attr('data-slot', null);
                            $remove.parent().html('');
                        });
                        if ($target.matches('.slot[data-slot="image"]')) {
                            $target.find('img').attr('src', _img.getAttribute('src'));
                            $target.find('img').attr('data-src', _this._fileData.filename);
                            $target.find('img').css('width', ($img.width < $target.innerWidth ? $img.width : $target.innerWidth) + 'px');
                            $target.find('img').attr('width', ($img.width < $target.innerWidth ? $img.width : $target.innerWidth) + '');
                            $target.find('img').css('height', 'auto');
                        }
                        else if ($target.matches('.slot') && !$target.matches('[data-slot]')) {
                            var $image = new $el('img');
                            $image.attr('src', _img.getAttribute('src'));
                            $image.attr('data-src', _this._fileData.filename);
                            $image.css('width', ($img.width < $target.innerWidth ? $img.width : $target.innerWidth) + 'px');
                            $image.attr('width', (imgWidth < $target.innerWidth ? imgWidth : $target.innerWidth) + '');
                            $image.css('height', 'auto');
                            $image.css('display', 'block');
                            $target.data('slot', 'image');
                            $target.html('');
                            $target.append($image);
                            $target.append($remove);
                        }
                        else if ($target.matches('.slot')) {
                            var imgWidth = $img.width;
                            var popit = new Application.Popup([
                                'div', { class: 'content' },
                                ['div', { class: 'title' }, '{t(slot-set.' + $target.data('slot') + ')}'],
                                ['div', { class: 'buttons' },
                                    ['span', {
                                            class: 'button bad',
                                            'data-name': 'remove',
                                            onclick: function () {
                                                var $image = new $el('img');
                                                $image.attr('src', _img.getAttribute('src'));
                                                $image.attr('data-src', _this._fileData.filename);
                                                $image.css('width', (imgWidth < $target.innerWidth ? imgWidth : $target.innerWidth) + 'px');
                                                $image.css('height', 'auto');
                                                $image.css('display', 'block');
                                                $image.attr('width', (imgWidth < $target.innerWidth ? imgWidth : $target.innerWidth) + '');
                                                $target.data('slot', 'image');
                                                $target.html('');
                                                $target.append($image);
                                                $target.append($remove);
                                                popit.close();
                                            }
                                        }, '{t(popup.ok)}'],
                                    ['span', { class: 'button', onclick: function () { popit.close(); } }, '{t(popup.cancel)}']
                                ]
                            ]);
                        }
                        $target.off('click');
                        moveable = null;
                    };
                });
                this._fileData = fileData;
                if (fileData) {
                    this.setFileData(fileData);
                }
            }
            Image.prototype.setFileData = function (fileData, content) {
                this.DOMELEMENT.find('[data-name="name"]').html(fileData.name);
                this.DOMELEMENT.find('[data-name="size"]').html(this.formatFileSize(fileData.size));
                this.DOMELEMENT.find('[data-name="preview"]').attr({ 'src': content, 'draggable': false, 'data-src': fileData.name });
                if (this.DOMELEMENT.find('[data-name="preview"]').width < this.DOMELEMENT.find('[data-name="preview"]').height) {
                    this.DOMELEMENT.find('[data-name="preview"]').addClass('portrait');
                }
                fileData.content = content;
                fileData.filename = fileData.name;
                this._fileData = fileData;
                return this;
            };
            Image.prototype.formatFileSize = function (filesize) {
                if (filesize === 0) {
                    return '0 B';
                }
                else {
                    var index = Math.floor(Math.log(filesize) / Math.log(1024));
                    var fsz = filesize / Math.pow(1024, index);
                    return fsz.toFixed(2) + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][index];
                }
            };
            Image.prototype.getFileData = function () {
                return this._fileData;
            };
            Image.resetDataSlot = function () { };
            return Image;
        }());
        FileManager.Image = Image;
    })(FileManager = Application.FileManager || (Application.FileManager = {}));
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Toolbar;
    (function (Toolbar) {
        var Button = (function () {
            function Button(editor, html, action) {
                this.DOMELEMENT = new $el('span');
                this.DOMELEMENT.html(html);
                this._action = action;
                this._editor = editor;
                this.DOMELEMENT.attr('draggable', 'false');
                this.DOMELEMENT.addClass('button');
            }
            Button.prototype.html = function (html) {
                this.DOMELEMENT.html(html);
            };
            Button.prototype.setAction = function (element, action) {
                var _this = this;
                if (this._action) {
                    if (this.DOMELEMENT.children('select').length) {
                        this.DOMELEMENT.children('select').off('change', this._action);
                    }
                    else {
                        this.DOMELEMENT.off('mousedown', this._action);
                    }
                }
                this._action = function (e) {
                    action.apply(element, [this, e, _this._editor]);
                    if (e.stopPropagation)
                        e.stopPropagation();
                    if (e.preventDefault)
                        e.preventDefault();
                    e.cancelBubble = true;
                    e.returnValue = false;
                    return false;
                };
                if (this.DOMELEMENT.children('select').length) {
                    this.DOMELEMENT.children('select').on('change', this._action);
                }
                else {
                    this.DOMELEMENT.on('mousedown', this._action);
                }
            };
            Button.prototype.getAction = function () {
                return this._action;
            };
            return Button;
        }());
        Toolbar.Button = Button;
    })(Toolbar = Application.Toolbar || (Application.Toolbar = {}));
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Toolbar;
    (function (Toolbar) {
        var Seperator = (function () {
            function Seperator() {
                this.DOMELEMENT = new $el('seperator');
            }
            return Seperator;
        }());
        Toolbar.Seperator = Seperator;
    })(Toolbar = Application.Toolbar || (Application.Toolbar = {}));
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Toolkit;
    (function (Toolkit) {
        var Button = (function () {
            function Button() {
                this.DOMELEMENT = new $el('img');
                this.DOMELEMENT.attr('draggable', 'false');
            }
            Button.prototype.setSources = function (src) {
                this.DOMELEMENT.attr('src', src);
            };
            Object.defineProperty(Button.prototype, "title", {
                get: function () {
                    return this.DOMELEMENT.prop('title');
                },
                set: function (title) {
                    this.DOMELEMENT.prop('title', title);
                },
                enumerable: true,
                configurable: true
            });
            Button.prototype.setAction = function (element, action, editor) {
                if (this._action) {
                    this.DOMELEMENT.off('mousedown', this._action);
                }
                this._action = function (e) {
                    action.apply(element, [this, e, editor]);
                };
                this.DOMELEMENT.on('mousedown', this._action);
            };
            Button.prototype.getAction = function () {
                return this._action;
            };
            return Button;
        }());
        Toolkit.Button = Button;
    })(Toolkit = Application.Toolkit || (Application.Toolkit = {}));
})(Application || (Application = {}));
var Application;
(function (Application) {
    var Toolkit;
    (function (Toolkit) {
        var Seperator = (function () {
            function Seperator() {
                this.DOMELEMENT = new $el('seperator');
            }
            return Seperator;
        }());
        Toolkit.Seperator = Seperator;
    })(Toolkit = Application.Toolkit || (Application.Toolkit = {}));
})(Application || (Application = {}));
