if (typeof config === "undefined") {
    var config = {};
}
var templates = null;
function getSelectedText() {
    var text = "";
    if (typeof window.getSelection != "undefined") {
        text = window.getSelection().toString();
    } else if (typeof document.selection != "undefined" && document.selection.type == "Text") {
        text = document.selection.createRange().text;
    }
    return text;
}
function GetHREF(){
   if (getSelectedText().length == 0) {
       var popup = new Application.Popup([
            'div', {class: 'content'}, 
                ['div', {class: 'title'}, '{t(select.text)}'],
                ['div', {class: 'buttons'}, 
                    ['span', {
                        class: 'button good',
                        onclick: function() {
                            popup.close();
                        }
                    }, '{t(popup.ok)}']
                ]
        ]);
       return null;
   }
   if ( document.selection ){
      var range = document.selection.createRange();
      var parent;
      
      if ( range.parentElement ){
         parent = range.parentElement();
         while ( parent.parentElement && parent.tagName != "A" )
            parent = parent.parentElement;
         
         if (parent.tagName == "A")
            return (parent.getAttribute("href"));
      }
   } else {
      var range = document.getSelection().getRangeAt(0).startContainer;
      var parent;
      
      if ( range.parentNode ){
         parent = range.parentNode;
         while ( parent.parentNode && parent.tagName != "A" )
            parent = parent.parentNode;
         
         if (parent.tagName == "A")
            return (parent.getAttribute("href"));
      }
   }
   return '';
}
var current_selection = null;
config.ToolBarButtons = [
    [{
        html: '<i class="fa fa-trash-o fa-fw" title="{t(toolbar.clear)}"></i>',
        contentCheck: function(_el, __el) {
            
        },
        action: function() {
            var popup = new Application.Popup([
                'div', {class: 'content'}, 
                    ['div', {class: 'title'}, '{t(clear.template.message)}'],
                    ['div', {class: 'buttons'}, 
                        ['span', {
                            class: 'button bad',
                            'data-name': 'remove',
                            onclick: function() {
                                $el.find('editor tr:not(:first-child)').remove();
                                popup.close();
                            }
                        }, '{t(popup.clear)}'],
                        ['span', {class: 'button', onclick: function() {popup.close();}}, '{t(popup.cancel)}']
                    ]
            ]);
        }
    },
    {
        html: '<i class="fa fa-folder-open-o fa-fw" title="{t(toolbar.open)}"></i>',
        contentCheck: function() {},
        action: function(e) {
            var popup = new Application.Dialog([
                'div', {class: 'content'}, 
                    ['div', {class: 'title'}, '{t(open.template)}'],
                    ['div', {class: 'templates'}, templates],
                    ['div', {class: 'buttons'}, 
                        ['span', {
                            class: 'button good',
                            onclick: function() {
                                var $selected = popup.DOMELEMENT.find('.selected');
                                if ($selected.length) {
                                    window.top.Monalyse.MailEditor.run('setTemplate', [$selected.attr('data-id')]);
                                    popup.close();
                                }
                            }
                        }, '{t(popup.save)}'],
                        ['span', {class: 'button bad', onclick: function() {
                            popup.close();
                        }}, '{t(popup.cancel)}']
                    ]
            ])
        }
    }],
    [{
        html: '<i class="fa fa-bold fa-fw" title="{t(toolbar.text.bold)}"></i>',
        contentCheck: function(_el, __el) {
            _el.attr('data-checked', document.queryCommandState('bold'));
        },
        action: function(e) {
            document.execCommand('bold', false, null);
            this.contentCheck($el.from(e));
        }
    },
    {
        html: '<i class="fa fa-italic fa-fw" title="{t(toolbar.text.italic)}"></i>',
        contentCheck: function(_el, __el) {
            _el.attr('data-checked', document.queryCommandState('italic'));
        },
        action: function(e) {
            document.execCommand('italic', false, null);
            this.contentCheck($el.from(e));
        }
    },
    {
        html: '<i class="fa fa-underline fa-fw" title="{t(toolbar.text.underline)}"></i>',
        contentCheck: function(_el, __el) {
            _el.attr('data-checked', document.queryCommandState('underline'));
        },
        action: function(e) {
            document.execCommand('underline', false, null);
            this.contentCheck($el.from(e));
        }
    },
    {
        html: '<i class="fa fa-strikethrough fa-fw" title="{t(toolbar.text.strikethrough)}"></i>',
        contentCheck: function(_el, __el) {
            _el.attr('data-checked', document.queryCommandState('strikethrough'));
        },
        action: function(e) {
            document.execCommand('strikethrough', false, null);
            this.contentCheck($el.from(e));
        }
    }],
    [{
        html: '<i class="fa fa-text-height fa-fw" title="{t(toolbar.text.fontsize)}"></i>'
    },{
        html: '<select></select>',
        load: function(_el) {
            $el.each([6,7,8,9,10,11,12,13,14,15,16,18,20,22,24,26,28,32,36,48,54,60,66,72,80,88,96], function(i, v) {
                _el.find('select').append('<option value="' + v + '">' + v + '</option>');
            });
        },
        contentCheck: function(_el, __el) {
            _el.find('option').each(function() {
                var $this = $el.from(this);

                var parentEl = null, sel;
                if (window.getSelection) {
                    sel = window.getSelection();
                    if (sel.rangeCount) {
                        parentEl = sel.getRangeAt(0).commonAncestorContainer;
                        if (parentEl.nodeType != 1) {
                            parentEl = parentEl.parentNode;
                        }
                    }
                } else if ( (sel = document.selection) && sel.type != "Control") {
                    parentEl = sel.createRange().parentElement();
                }
                
                try {
                    fontsize = window.getComputedStyle(parentEl, null).getPropertyValue('font-size');
                    fontsize = fontsize.substr(0, fontsize.length-2);
                    if ($this.val() == fontsize) {
                        $this.attr('selected', 'selected');
                        _el.find('select').val(fontsize);
                    } else {
                        $this.attr('selected', null);
                    }
                } catch (e) {
                    
                }
            });
        },
        action: function(e) {     
            var $target = $el.from(e);
            document.execCommand('fontSize', false, 7);
            var $font = $el.find('font[size="7"]');
            $font.removeAttribute('size').css('font-size', e.value  + 'px');
        }
    }],
    [{
        html: '<i class="fa fa-font fa-fw" title="{t(toolbar.text.fontfamily)}"></i>'
    },{
        html: '<select></select>',
        load: function(_el) {
            var fontsList = ArrayToHTML.parse(
                ['option', {value: 'Arial, Helvetica, sans-serif'}, 'Arial'],
                ['option', {value: '"Courier New", Courier, monospace'}, 'Courier New'],
                ['option', {value: '"Georgia", serif'}, 'Georgia'],
                ['option', {value: 'Tahoma, Geneva, sans-serif'}, 'Tahoma'],
                ['option', {value: 'Helvetica, Arial, sans-serif'}, 'Helvetica'],
                ['option', {value: '"Times New Roman", Times, serif'}, 'Times New Roman'],
                ['option', {value: '"Trebuchet MS", Helvetica, sans-serif'}, 'Trebuchet MS'],
                ['option', {value: 'Verdana, Geneva, sans-serif'}, "Verdana"]
            );
            _el.find('select').childNodes(fontsList);
        },
        contentCheck: function(_el, __el) {
            _el.find('option').each(function() {
                var $this = $el.from(this);

                var parentEl = null, sel;
                if (window.getSelection) {
                    sel = window.getSelection();
                    if (sel.rangeCount) {
                        parentEl = sel.getRangeAt(0).commonAncestorContainer;
                        if (parentEl.nodeType != 1) {
                            parentEl = parentEl.parentNode;
                        }
                    }
                } else if ( (sel = document.selection) && sel.type != "Control") {
                    parentEl = sel.createRange().parentElement();
                }
                
                try {
                    fontname = window.getComputedStyle(parentEl, null).getPropertyValue('font-family');
                    if ($this.val() == fontname) {
                        $this.attr('selected', 'selected');
                        _el.find('select').val(fontname);
                    } else {
                        $this.attr('selected', null);
                    }
                } catch (e) {
                    
                }
            });
        },
        action: function(e) {
            var $target = $el.from(e);
            document.execCommand('fontName', false, e.value);
        }
    }],
    /*[{
        html: '<i class="fa fa-undo fa-fw" title="{t(toolbar.actions.undo)}"></i>',
        contentCheck: function(_el) {
            _el.addClass('action');
            if (window.top.Monalyse.MailEditor.has('checkUndo')) {
                _el.attr('data-checked', window.top.Monalyse.MailEditor.run('checkUndo') ? 'true' : 'false');
            } else {
                _el.attr('data-checked', 'false');
            }
        },
        action: function(e) {
            if (window.top.Monalyse.MailEditor.run('checkUndo')) {
                window.top.Monalyse.MailEditor.run('undo');
            }
        }
    },{
        html: '<i class="fa fa-repeat fa-fw" title="{t(toolbar.actions.redo)}"></i>',
        contentCheck: function(_el) {
            _el.addClass('action');
            if (window.top.Monalyse.MailEditor.has('checkRedo')) {
                _el.attr('data-checked', window.top.Monalyse.MailEditor.run('checkRedo') ? 'true' : 'false');
            } else {
                _el.attr('data-checked', 'false');
            }
        },
        action: function(e) {
            if (window.top.Monalyse.MailEditor.run('checkRedo')) {
                window.top.Monalyse.MailEditor.run('redo');
            }
        }
    }],*/
    [{
        html: '<i class="fa fa-align-left fa-fw" data-name="align-left" data-linked="align-right, align-center, align-justify" title="{t(toolbar.align.left)}"></i>',
        contentCheck: function(_el, __el) {
            _el.attr('data-checked', document.queryCommandState('justifyLeft'));
        },
        linked: function(e) {
            var selector = [];
            var linked = e.find('i').attr('data-linked').split(',');
            for (var i = 0; i < linked.length; i++) {
                selector.push('[data-name="' + linked[i]+ '"]');
            }
            
            return $el.find(selector.join(',')).parent();
        },
        action: function(e) {
            document.execCommand('justifyLeft', false, null);
            this.contentCheck($el.from(e));
            this.linked($el.from(e)).attr('data-checked', null);
        }
    },{
        html: '<i class="fa fa-align-center fa-fw" data-name="align-center" data-linked="align-left, align-right, align-justify" title="{t(toolbar.align.center)}"></i>',
        contentCheck: function(_el, __el) {
            _el.attr('data-checked', document.queryCommandState('justifyCenter'));
        },
        linked: function(e) {
            var selector = [];
            var linked = e.find('i').attr('data-linked').split(',');
            for (var i = 0; i < linked.length; i++) {
                selector.push('[data-name="' + linked[i]+ '"]');
            }
            
            return $el.find(selector.join(',')).parent();
        },
        action: function(e,event, editor) {
            if (editor.SELECTED_DOMELEMENT.find('.slot').matches('[data-slot="image"]')) {
                editor.SELECTED_DOMELEMENT.find('.slot').parent().css('text-align', 'center');
            } else {
                document.execCommand('justifyCenter', false, null);
            }
            this.contentCheck($el.from(e));
            this.linked($el.from(e)).attr('data-checked', null);
        }
    },{
        html: '<i class="fa fa-align-right fa-fw" data-name="align-right" data-linked="align-left, align-center, align-justify" title="{t(toolbar.align.right)}"></i>',
        contentCheck: function(_el, __el) {
            _el.attr('data-checked', document.queryCommandState('justifyRight'));
        },
        linked: function(e) {
            var selector = [];
            var linked = e.find('i').attr('data-linked').split(',');
            for (var i = 0; i < linked.length; i++) {
                selector.push('[data-name="' + linked[i]+ '"]');
            }
            
            return $el.find(selector.join(',')).parent();
        },
        action: function(e) {
            document.execCommand('justifyRight', false, null);
            this.contentCheck($el.from(e));
            this.linked($el.from(e)).attr('data-checked', null);
        }
    },{
        html: '<i class="fa fa-align-justify fa-fw" data-name="align-justify" data-linked="align-left, align-center, align-right" title="{t(toolbar.align.justify)}"></i>',
        contentCheck: function(_el, __el) {
            _el.attr('data-checked', document.queryCommandState('justifyFull'));
        },
        linked: function(e) {
            var selector = [];
            var linked = e.find('i').attr('data-linked').split(',');
            for (var i = 0; i < linked.length; i++) {
                selector.push('[data-name="' + linked[i]+ '"]');
            }
            
            return $el.find(selector.join(',')).parent();
        },
        action: function(e) {
            document.execCommand('justifyFull', false, null);
            this.contentCheck($el.from(e));
            this.linked($el.from(e)).attr('data-checked', null);
        }
    }],
    [{
        html: '<i class="fa fa-link fa-fw" title="{t(toolbar.link.add)}"></i>',
        linkNo: null,
        action: function(e) {
            this.linkNo = Math.floor(Math.random() * 1000);
            var _this = this;
            var href = GetHREF();
            if (href !== null) {
                document.execCommand('unlink');
                document.execCommand('createLink', true, '#' + this.linkNo);
                var popup = new Application.Popup([
                    'div', {class: 'content'}, 
                        ['div', {class: 'title'}, '{t(popup.url.title)}'],
                        ['div', {class: 'field'},
                            ['input', {
                                'data-name' : 'url',
                                oninput: function() {
                                    if (this.value.length > 0) {
                                        popup.DOMELEMENT.find('[data-name="save"]').addClass('good');
                                    } else {
                                        popup.DOMELEMENT.find('[data-name="save"]').removeClass('good');
                                    }
                                },
                                value: href
                            }]
                        ],
                        ['div', {class: 'buttons'}, 
                            ['span', {
                                class: 'button',
                                'data-name': 'save',
                                onclick: function() {
                                    if (popup.DOMELEMENT.find('[data-name="url"]').val().length) {
                                        $el.find('[href="#' + _this.linkNo + '"]').attr('href', popup.DOMELEMENT.find('[data-name="url"]').val());
                                        popup.close();
                                    }
                                }
                            }, '{t(popup.save)}'],
                            ['span', {class: 'button bad', onclick: function() {
                                if (href !== '') {
                                    $el.find('[href="#' + _this.linkNo + '"]').attr('href', href);
                                    popup.close();
                                } else {
                                    var $obj = $el.find('[href="#' + _this.linkNo + '"]');
                                    if ($obj.length) {
                                        $obj.outerHTML($obj.html());
                                    }
                                    popup.close();
                                }
                            }}, '{t(popup.cancel)}']
                        ]
                ]);
            }
        }
    },{
        html: '<i class="fa fa-unlink fa-fw" title="{t(toolbar.link.remove)}"></i>',
        action: function(e){
            document.execCommand('unlink');
        }
    }]
];