if (typeof config === "undefined") {
    var config = {};
}

config.Properties = {
    'color': {
        'default': '#0000ff',
        'joe': null,
        'initialize': function(input) {
            var $input = $el.from(input);
            var $colorDIV = new $el('div') ;
            this.joe = colorjoe.rgb($colorDIV.get(0), $input.val(),  [
                 'currentColor',
                 ['fields', {space: 'RGB', limit: 255, fix: 0}],
                 'hex']);
            $input.after($colorDIV);
            $input.on('click', function(e) {
               $input.addClass('focus');
               e.stopPropagation();
            });
            this.joe.on("change", function(color) {
                $input.val(color.hex());
                $input.css('color', color.hex()).css('background-color', color.hex());
                $input.trigger("change");
            });
            $el.from(document.documentElement).on('mousedown', function(e) {
               if (e) {
                var close = false;
                var path = [];
                var hasParent = e.target;
                while (hasParent.parentNode) {
                    path.push(hasParent);
                    hasParent = hasParent.parentNode;
                }
                path.push(hasParent);
                for(var i = 0; i < path.length-2; i++) {
                    if (!close) {
                        close = $el.from(path[i]).hasClass('colorPicker');
                    }
                }
                if (!close) { $input.removeClass('focus'); }
               } else {
                   $input.removeClass('focus');
               }
            });
            window.addEventListener('blur', function() {
               $input.removeClass('focus'); 
            });
        },
        'setValue': function(element, input) {
            var $input = $el.from(input);
            element.css('color', input.val());
            $input.css('color', input.val()).css('background-color', input.val());
        },
        'getValue': function(element, input) {
            this.joe.set(element.css('color'));
        }
    },
    'background': {
        'default': '#0000ff',
        'joe': null,
        'initialize': function(input) {
            var $input = $el.from(input);
            var $colorDIV = new $el('div') ;
            this.joe = colorjoe.rgb($colorDIV.get(0), $input.val(),  [
                 'currentColor',
                 ['fields', {space: 'RGB', limit: 255, fix: 0}],
                 'hex']);
            $input.after($colorDIV);
            $input.on('click', function(e) {
               $input.addClass('focus');
               e.stopPropagation();
            });
            this.joe.on("change", function(color) {
                $input.val(color.hex());
                $input.css('color', color.hex()).css('background-color', color.hex());
                $input.trigger("change");
            });
            $el.from(document.documentElement).on('mousedown', function(e) {
               if (e) {
                var close = false;
                var path = [];
                var hasParent = e.target;
                while (hasParent.parentNode) {
                    path.push(hasParent);
                    hasParent = hasParent.parentNode;
                }
                path.push(hasParent);
                for(var i = 0; i < path.length-2; i++) {
                    if (!close) {
                        close = $el.from(path[i]).hasClass('colorPicker');
                    }
                }
                if (!close) { $input.removeClass('focus'); }
               } else {
                   $input.removeClass('focus');
               }
            });
            window.addEventListener('blur', function() {
               $input.removeClass('focus'); 
            });
        },
        'setValue': function(element, input) {
            var $input = $el.from(input);
            element.css('background', input.val());
            $input.css('color', input.val()).css('background-color', input.val());
        },
        'getValue': function(element, input) {
            this.joe.set(element.css('background-color'));
        }
    },
    'padding': {
        'default': '0',
        'initialize': function(input) {
            
        },
        setPadding: function(element, value, top, right, bottom, left) {
            var $table = new $el('table');
            $table.attr({style: "width: "+element.width+"px;", cellspacing:"0", cellpadding:"0", border:"0"})
            var $toprow = new $el('tr');
            if (top != 0) {
                if (left != 0) {
                    $toprow.append('<td height="' + top + '" style="margin: 0; padding: 0; font-size:0; line-height:0;"></td>');
                }
                $toprow.append('<td height="' + top + '" style="margin: 0; padding: 0; font-size:0; line-height:0;"></td>');
                if (right != 0) {
                    $toprow.append('<td height="' + top + '" style="margin: 0; padding: 0; font-size:0; line-height:0;"></td>');
                }
            }
            if(bottom != 0) {
                var $bottomrow = new $el('tr');
                if (left != 0) {
                    $bottomrow.append('<td height="' + bottom + '" style="margin: 0; padding: 0; font-size:0; line-height:0;"></td>');
                }
                $bottomrow.append('<td height="' + bottom + '" style="margin: 0; padding: 0; font-size:0; line-height:0;"></td>');
                if (right != 0) {
                    $bottomrow.append('<td height="' + bottom + '" style="margin: 0; padding: 0; font-size:0; line-height:0;"></td>');
                }    
            }
            var $middle = new $el('tr');
            if (left != 0) {
                $middle.append('<td width="' + left + '" style="margin: 0; padding: 0; font-size:0; line-height:0;"></td>');
            }
            $middle.append('<td width="' + (element.width - left - right) + '"  style="margin: 0; padding: 0; ">'+element.find('.slot').outerHTML()+'</td>');
            if (right != 0) {
                $middle.append('<td width="' + right + '" style="margin: 0; padding: 0; font-size:0; line-height:0;"></td>');
            }
            if (top != 0) {
                $table.append($toprow);
            }
            $table.append($middle);
            if (bottom != 0) {
                $table.append($bottomrow);
            }
            if (top == 0 && left == 0 && right == 0 && bottom == 0) {
                element.html(element.find('.slot').outerHTML());
            } else {
                element.html('').append($table);
            }
            element.attr('data-padding', value);
        },
        'setValue': function(element, input, set) {
            var value = input;
            if (typeof set === "undefined" || set === false) {
                value = input.val();
            }
            var splittedVal = value.split(' ');
        
            if (splittedVal.length == 3) {
                splittedVal.push(splittedVal[1]);
            } else if (splittedVal.length == 2) {
                splittedVal.push(splittedVal[0]);
                splittedVal.push(splittedVal[1]);
            } else {
                splittedVal.push(splittedVal[0]);
                splittedVal.push(splittedVal[0]);
                splittedVal.push(splittedVal[0]);
            }
            this.setPadding.apply(null, [element, value].concat(splittedVal));
        },
        'getValue': function(element, input) {
            if (element.get(0).tagName === 'EDITOR') {
                input.attr('disabled', 'disabled');
                input.val('');
            } else {
                input.attr('disabled', null);
                input.val(element.attr('data-padding') ? element.attr('data-padding') : 0);
            }
        }
    }
};