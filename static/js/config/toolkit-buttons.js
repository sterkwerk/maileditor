if (typeof config === "undefined") {
    var config = {};
}
var counter = 0;
function initializeTextInput($target) {
    counter++;
    var $_textbox = new $el('div');
    $_textbox.attr({'contenteditable': 'true', 'spellcheck': 'false'});
    $_textbox.html('<font face="Arial, Helvetica, sans-serif">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus molestie libero vel leo sagittis, vel molestie diam semper. Suspendisse cursus luctus blandit. Vestibulum commodo lorem risus. Pellentesque tristique id erat eget porta. </font></div>');
    $_textbox.css('width', '100%').css('min-height', '20px');

    $target.html($_textbox.outerHTML());
    $target.attr('data-slot', 'textarea'); 
    $target.off('click');
    document.execCommand('styleWithCSS', false, false);
    config.Properties.padding.setValue($target.parent(), '20' ,true);
}
Application.FileManager.Image.resetDataSlot = function($this) {
    console.log($this);
    $this.off('click').on('click', function() {
        var $target = $el.from(this);
        initializeTextInput($target);
    });
}
config.ToolKitButtons = [
    [
        {
            title: 'Single column',
            name: 'single-column',
            icons: ['editor/icons/grid_full.png'],
            action: function(e, a, editor) {
                var _this = this;
                $el.get('editor/prebaked/single-row.html', function(data) {
                    var moveable = new Application.Draggable(data, Application.Draggable.Context, _this);
                    moveable.release = function(e) {
                        var $target = $el.from(e.target);
                        var html = ArrayToHTML.parse(['tr', null, 
                                        ['td', {'width': 600}, 
                                            ['div', {'class' : 'remove', 'onclick': function(e) {
                                                e.stopPropagation();
                                                $el.from(this).parent().parent().remove();
                                            }}]
                                            , data]]);
                        if ($target.parents(_this.destination).length > 0 || $target.matches(_this.destination) ) {
                            if ($target.parents(_this.destination).length > 0) {
                                $target.parents(_this.destination).afterArray(html);     
                            } else {
                                $target.afterArray(html);
                            }
                        } else {
                            $el.find('body > editor > table > tbody').appendArray(html);
                        }
                        $el.find('.slot:not([data-slot])').off('click').on('click', function() {
                            var $target = $el.from(this);
                            initializeTextInput($target);
                        });
                        editor.reattachEvents();
                    };
                });
            },
            allowed: ['editor > table > tbody > tr > td'],
            destination: 'editor > table > tbody > tr'
        }, {
            title: 'Duo column',
            name: 'duo-column',
            icons: ['editor/icons/grid_halve.png'],
            action: function(e, a, editor) {
                var _this = this;
                $el.get('editor/prebaked/duo-row.html', function(data) {
                    var moveable = new Application.Draggable(data, Application.Draggable.Context, _this);
                    moveable.release = function(e) {
                        var $target = $el.from(e.target);
                        var html = ArrayToHTML.parse(['tr', null, 
                                        ['td', {'width': 600}, 
                                            ['div', {'class' : 'remove', 'onclick': function(e) {
                                                e.stopPropagation();
                                                $el.from(this).parent().parent().remove();
                                            }}]
                                            , data]]);
                        if ($target.parents(_this.destination).length > 0 || $target.matches(_this.destination) ) {
                            if ($target.parents(_this.destination).length > 0) {
                                $target.parents(_this.destination).afterArray(html);     
                            } else {
                                $target.afterArray(html);
                            }
                        } else {
                            $el.find('body > editor > table > tbody').appendArray(html);
                        }
                        $el.find('.slot:not([data-slot])').off('click').on('click', function() {
                            var $target = $el.from(this);
                            initializeTextInput($target);
                        });
                        editor.reattachEvents();
                    };
                });
            },
            allowed: ['editor > table > tbody > tr > td'],
            destination: 'editor > table > tbody > tr'
        }, {
            title: 'Triple column',
            name: 'triple-column',
            icons: ['editor/icons/grid_three.png'],
            action: function(e, a, editor) {
                var _this = this;
                $el.get('editor/prebaked/triple-row.html', function(data) {
                    var moveable = new Application.Draggable(data, Application.Draggable.Context, _this);
                    moveable.release = function(e) {
                        var $target = $el.from(e.target);
                        var html = ArrayToHTML.parse(['tr', null, 
                                        ['td', {'width': 600}, 
                                            ['div', {'class' : 'remove', 'onclick': function(e) {
                                                e.stopPropagation();
                                                $el.from(this).parent().parent().remove();
                                            }}]
                                            , data]]);
                        
                        if ($target.parents(_this.destination).length > 0 || $target.matches(_this.destination) ) {
                            if ($target.parents(_this.destination).length > 0) {
                                $target.parents(_this.destination).afterArray(html);     
                            } else {
                                $target.afterArray(html);
                            }
                        } else {
                            $el.find('body > editor > table > tbody').appendArray(html);
                        }
                        $el.find('.slot:not([data-slot])').off('click').on('click', function() {
                            var $target = $el.from(this);
                            initializeTextInput($target);
                        });
                        editor.reattachEvents();
                    };
                });
            },
            allowed: ['editor > table > tbody > tr > td'],
            destination: 'editor > table > tbody > tr'
        }, {
            title: 'Quattro column',
            name: 'quattro-column',
            icons: ['editor/icons/grid_quarter.png'],
            action: function(e, a, editor) {
                var _this = this;
                $el.get('editor/prebaked/quatro-row.html', function(data) {
                    var moveable = new Application.Draggable(data, Application.Draggable.Context, _this);
                    moveable.release = function(e) {
                        var $target = $el.from(e.target);
                        var html = ArrayToHTML.parse(['tr', null, 
                                        ['td', {'width': 600}, 
                                            ['div', {'class' : 'remove', 'onclick': function(e) {
                                                e.stopPropagation();
                                                $el.from(this).parent().parent().remove();
                                            }}]
                                            , data]]);
                        if ($target.parents(_this.destination).length > 0 || $target.matches(_this.destination) ) {
                            if ($target.parents(_this.destination).length > 0) {
                                $target.parents(_this.destination).afterArray(html);     
                            } else {
                                $target.afterArray(html);
                            }
                        } else {
                            $el.find('body > editor > table > tbody').appendArray(html);
                        }
                        $el.find('.slot:not([data-slot])').off('click').on('click', function() {
                            var $target = $el.from(this);
                            initializeTextInput($target);
                        });
                        editor.reattachEvents();
                    };
                });
            },
            allowed: ['editor > table > tbody > tr > td'],
            destination: 'editor > table > tbody > tr'
        }
    ]
];