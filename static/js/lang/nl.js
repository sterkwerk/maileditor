Application.Language.register("nl", {
    "upload": {
        "file": "Bestand uploaden"
    },
    "font": {
        "size": "Text grootte",
        "family": "Lettertype"
    },
    "slot": "Target",
    "files": "Bestanden",
    "properties": {
        "color": "Tekstkleur",
        "background": "Achtergrond",
        "padding": "Padding"
    },
    "popup": {
        "url": {
            "title": "Link"
        },
        "save": "Opslaan",
        "cancel": "Annuleren",
        "remove": "Verwijderen",
        "clear": "Legen",
        "ok": "Oké"
    },
    "clear.template.message": "Wilt u dit ontwerp legen?",
    "remove.file.message": "Wilt u dit bestand verwijderen?",
    "select.text": "U moet eerst tekst selecteren.",
    "toolbar": {
        "clear": "Legen",
        "open": "Open Sjabloon",
        "text": {
            "bold": "Vet",
            "italic": "Schuin",
            "underline": "Onderstreept",
            "strikethrough": "Doorgestreept",
            "fontsize": "Tekstgrootte",
            "fontfamily": "Lettertype"
        },
        "align": {
            "left": "Links uitlijnen",
            "center": "Gecentreerd uitlijnen",
            "right": "Rechts uitlijnen",
            "justify": "Gevuld uitlijnen"
        },
        "link": {
            "add": "Voeg link toe",
            "remove": "Verwijder link"
        },
        "actions": {
            "undo": "Ongedaan maken",
            "redo": "Herhalen"
        }
    },
    "open": {
        "template": "Kies een sjabloon"
    },
    "slot-set": {
        "textarea": "Dit target bevat al tekst. Wilt u deze tekst vervang door de afbeelding?"
    }
});