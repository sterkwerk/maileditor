Application.Language.register("en", {
    "upload": {
        "file": "Upload file"
    },
    "font": {
        "size": "Font size",
        "family": "Font family"
    },
    "slot": "Slot",
    "files": "Files",
    "properties": {
        "color": "Color",
        "background": "Background",
        "padding": "Padding"
    },
    "popup": {
        "url": {
            "title": "Link"
        },
        "save": "Save",
        "cancel": "Cancel",
        "remove": "Remove",
        "clear": "Clear",
        "ok": "OK"
    },
    "clear.template.message": "Are you sure to clear this design?",
    "remove.file.message": "Are you sure to remove this file?",
    "select.text": "You have to select some text first.",
    "toolbar": {
        "clear": "Clear",
        "open": "Open Template",
        "text": {
            "bold": "Bold",
            "italic": "Italic",
            "underline": "Underline",
            "strikethrough": "Strikethrough",
            "fontsize": "Font size",
            "fontfamily": "Font family"
        },
        "align": {
            "left": "Align left",
            "center": "Align center",
            "right": "Align right",
            "justify": "Justify"
        },
        "link": {
            "add": "Add link",
            "remove": "Remove link"
        },
        "actions": {
            "undo": "Undo",
            "redo": "Redo"
        }
    },
    "open": {
        "template": "Pick a template"
    },
    "slot-set": {
        "textarea": "This slot already contains text. Replace it with the image?"
    }
});