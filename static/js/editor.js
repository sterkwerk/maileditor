$el.ready(function() {
   $el.extendsHTMLwithTranslate(Application.Language.translate);
   ArrayToHTML.extendsHTMLwithTranslate(Application.Language.translate);
   var app = new Application();
   Application.Popup.document = app.DOMELEMENT; 
   Application.Dialog.document = app.DOMELEMENT; 
   $el.from(document.documentElement).attr('lang', Application.Language.language);
   var editor = new Application.Editor();
   var toolkit = new Application.Toolkit();
   var toolbar = new Application.Toolbar();
   var filemanager = new Application.FileManager();
   var properties = new Application.Properties(editor, config.Properties);
   app.title = 'MonAlyse Mail Editor';
   toolkit.loadConfig(editor, config.ToolKitButtons);

   toolbar.loadConfig(editor, config.ToolBarButtons);
   Application.Draggable.Context = app.DOMELEMENT;
   app.addElement(editor.DOMELEMENT, toolkit.DOMELEMENT, filemanager.DOMELEMENT, toolbar.DOMELEMENT, properties.DOMELEMENT);
   editor.SELECTED_DOMELEMENT = $el.from(editor.DOMELEMENT);
   editor.triggerWatch('SELECTED_DOMELEMENT');
   var outerHTML, outerText;
   var newFileGet = function() {};
   var remFileGet = function() {};

   var tmpDisableTriggerWatch = false;
   var watchHTMLEditor = function() {
        var _editor = editor.DOMELEMENT.clone();
        
        _editor.find('.placeholder').each(function() {
            this.parentNode.parentNode.removeChild(this.parentNode);
        });
        _editor.find('div.slot, div[contenteditable]').each(function() {
            var div = $el.from(this);
            div.before(div.children());    
        }).remove();
        _editor.find('div[style]').each(function() {
           var div = $el.from(this);
           
           if (div.parent().attr('style')) {
           var style = div.parent().attr('style').substr(-1, 1) == ';' ? div.parent().attr('style') + ' ' : div.parent().attr('style') + '; ';
           div.parent().attr('style',  style+ div.attr('style'));
           }
           div.before(div.children());  
        }).remove();
        _editor.find('[data-src]').attr('data-src', null);
        _editor.find('div.remove').remove();
        _editor.find('span[style]').each(function() {
            var div = $el.from(this);
            if (div.parent().attr('style')) {
                 var style = div.parent().attr('style').substr(-1, 1) == ';' ? div.parent().attr('style') + ' ' : div.parent().attr('style') + '; ';
                 var parentNewStyle = style + div.attr('style');
                 parentNewStyle = parentNewStyle.replace(/text-align\s*:\s*inherit\s*;?/, '');
                 div.parent().attr('style',  parentNewStyle);
            }
            div.before(div.children());  
         }).remove();
        _editor.find('.content').attr('class', null);
        _editor.find('[data-padding]').attr('data-padding', null);
        outerHTML = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\
<html>\
<head>\
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">\
    <title></title>\
</head>\
<body style="margin: 0; padding: 0; text-align: left; ' + _editor.attr('style') + '"><center>\
    ' + _editor.html() + '\
</center></body>\
</html>';
        outerText = _editor.text().replace(/^\s+|\s+$/g,'').replace(/\n\s+/g, '\n\n');
        editor.reattachEvents();
    };
    if (typeof window.top.Monalyse === "undefined") {
        window.top.Monalyse = {};
    }
    if (typeof window.top.Monalyse.MailEditor === "undefined") {
        window.top.Monalyse.MailEditor = {};
    }
    window.top.Monalyse.MailEditor.outputHTML = function() {
        watchHTMLEditor();
        return outerHTML;
    };
    window.top.Monalyse.MailEditor.outputText = function() {
        watchHTMLEditor();
        return outerText;
    };
    window.top.Monalyse.MailEditor.editorHTML = function() {
        var clone = editor.DOMELEMENT.clone();
        clone.find('.remove').remove();
        return clone.outerHTML();
    };
    filemanager.onNewFile(function() {
        newFileGet.apply(null, arguments);
    });
    filemanager.onRemFile(function() {
        remFileGet.apply(null, arguments);
    });
    window.top.Monalyse.MailEditor.onNewFile = function(funct) {
        newFileGet = funct;
    };
    window.top.Monalyse.MailEditor.onRemFile = function(funct) {
        remFileGet = funct;
    };
    window.top.Monalyse.MailEditor.refresh = function() {
        location.reload();
    };
    window.top.Monalyse.MailEditor.debug = {
        editor: editor
    };
    window.top.Monalyse.MailEditor.load = function(html) {
        var e = new $el('div');
        tmpDisableTriggerWatch = true;
        e.append(html);

        editor.DOMELEMENT.html(e.find('editor').html());
        if (e.find('editor').attr('style')) {
            editor.DOMELEMENT.attr('style', e.find('editor').attr('style'));
        } else {
            editor.DOMELEMENT.attr('style', ' ');
        }
        editor.DOMELEMENT.find('img').each(function() {
            var $remove = new $el('div');
            $remove.addClass('remove');
            $remove.on('click', function(e) {
                e.stopPropagation();
                var p = $remove.parent().parent();
                p.off('click').on('click', function() {
                    var $target = $el.from(this);
                    initializeTextInput($target);
                });
                $remove.parent().attr('data-slot', null);
                $remove.parent().html('');
            });
            $el.from(this).after($remove);
        });
        editor.DOMELEMENT.parent().find('editor > table > tbody > tr:not(:first-child) > td').each(function() {
            var $removeRow = new $el('div');
            $removeRow.addClass('remove');
            $removeRow.on('click', function(e) {
                e.stopPropagation();
                $el.from(this).parent().parent().remove();
            });
            $el.from(this).prepend($removeRow);
        });
        tmpDisableTriggerWatch = false;
    };
    window.top.Monalyse.MailEditor.loadFiles = function(files) {
        filemanager.loadFiles(files);
    };
    var triggerWatched = function() {};
    window.top.Monalyse.MailEditor.onTriggerSave = function(w) {
        triggerWatched = w;
    };
    window.top.Monalyse.MailEditor.loadTemplates = function(list) {
        var items = ['div', {class: 'items'}];
        for(var i = 0; i < list.length; i++) {
            var item = ['div', {class: 'item', dataId: list[i].id , onclick: function() { var $this = $el.from(this); $this.parent().find('.selected').removeClass('selected'); $this.addClass('selected'); }}, ['img', {src: list[i].image}], ['span', null, list[i].name]];
            items.push(item);
        }
        templates = items;
    };
    window.top.Monalyse.MailEditor.version = '0.1.33';
    editor.DOMELEMENT.on('propertychange', watchHTMLEditor);
    editor.DOMELEMENT.on('DOMSubtreeModified', watchHTMLEditor);
    editor.DOMELEMENT.on('propertychange', function() { if (!tmpDisableTriggerWatch) { triggerWatched() } });
    editor.DOMELEMENT.on('DOMSubtreeModified', function() { if (!tmpDisableTriggerWatch) { triggerWatched() }});
    window.top.Monalyse.MailEditor.getFiles = function() {
        return filemanager.getFiles(); 
    };
    window.top.Monalyse.MailEditor.changeImage = function(image, realPath) {
        editor.DOMELEMENT.find('img[data-src="' + image+ '"]').attr('src', realPath);
        filemanager.DOMELEMENT.find('img[data-src="' + image+ '"]').attr('src', realPath);
    };
    
});
