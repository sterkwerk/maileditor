if (typeof window.top.Monalyse === "undefined") {
    window.top.Monalyse = {};
}
if (typeof window.top.Monalyse.MailEditor === "undefined") {
    window.top.Monalyse.MailEditor = {};
}
window.top.Monalyse.MailEditor._actions = {};
window.top.Monalyse.MailEditor.has = function(action) {
    return window.top.Monalyse.MailEditor._actions.hasOwnProperty(action);
};
window.top.Monalyse.MailEditor.run = function(action, args) {
    return window.top.Monalyse.MailEditor._actions[action].apply(null, args);
};
window.top.Monalyse.MailEditor.set = function(action, func) {
    window.top.Monalyse.MailEditor._actions[action] = func;
};