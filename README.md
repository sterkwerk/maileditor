# Monalyse Mail Editor
This is a javascript application to design HTML emails with in the browser.

## How to use?
Embed with:
```html
<iframe src="/bower_components/maileditor/index.html" border="0" width="100%" height="600"></iframe>
```
Or:
```html
<iframe src="/javascript/vendor/maileditor/index.html" border="0" width="100%" height="600"></iframe>
```

Recieve HTML with:
```javascript
Monalyse.MailEditor.outputHTML()
```

Recieve Text with:
```javascript
Monalyse.MailEditor.outputText()
```

Recieve EditorHTML with:
```javascript
Monalyse.MailEditor.editorHTML()
```

Load EditorHTML with:
```javascript
Monalyse.MailEditor.load(editorhtml)
```

On new file uploaded:
```javascript
Monalyse.MailEditor.onNewFile(function(fileInfo) {
    //callback
});
```

On remove file:
```javascript
Monalyse.MailEditor.onRemFile(function(fileInfo) {
    //callback
});
```

Refresh Editor: (will lose anything unsaved)
```javascript
Monalyse.MailEditor.refresh();
```

Load attached files:
```javascript
Monalyse.MailEditor.loadFiles(files);
```

Get version:
```javascript
Monalyse.MailEditor.version
```