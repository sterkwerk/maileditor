interface HTMLElement {
    attachEvent(event: string, listener: EventListener): boolean;
    detachEvent(event: string, listener: EventListener): void;
}
class ArrayToHTML {
    private static translateFunction: any = null;
    static extendsHTMLwithTranslate(translateFunction:any) {
        ArrayToHTML.translateFunction = translateFunction;
    }
    static parse(...arr: Array<any>) {
        var html: Array<any> = [];
        
        for(var i = 0; i < arr.length; i++) {
            var $elm: any = arr[i];
            var element: $el = new $el($elm[0]);
            if ($elm.length > 1) {
                if ($elm[1] !== null) {
                    var $attributes = $elm[1];
                    for (var x in $attributes) {
                        if ($attributes.hasOwnProperty(x)) {
                            var $attrval = $attributes[x];
                            x = x.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
                            if (x.substr(0, 2) == 'on') {
                                element.on(x.substr(2), $attrval);
                            } else {
                                if (typeof $attrval === "string") {
                                    if (ArrayToHTML.translateFunction) {
                                        $attrval = (<string>$attrval).replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                                            var translated: string = ArrayToHTML.translateFunction(content);
                                            if (translated === null) {
                                                return '{t(' + content + ')}';
                                            }
                                            return translated;
                                        });
                                    }
                                }
                                element.attr(x, $attrval);
                            }
                        }
                    }
                }
            }
            if ($elm.length > 2) {
                for (var o = 0; o < $elm.length - 2; o++) {
                    var $cElm = $elm[o+2];
                    if ($cElm instanceof Array) {
                        $cElm = ArrayToHTML.parse($cElm);
                        for (var p = 0; p < $cElm.length; p++) {
                            element.append($cElm[p]);
                        }
                    } else if (typeof $cElm === "string") {
                        element.append($cElm);
                    }
                }
            }
            html.push(element);
        }
        
        return html;
    }
    toString(Elm:Array<any>) {
        var html: Array<any> = [];
        for(var i = 0; i< Elm.length; i++) {
            html.push(Elm[i].outerHTML);
        }
        return html.join('');
    }
}