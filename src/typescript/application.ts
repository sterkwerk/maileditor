/// <reference path="el.ts" />
class Application {
	public DOMELEMENT:$el;
	protected _linked: Array<any> = [];
	public constructor() {      
        this.DOMELEMENT = $el.find('body');
	}
	public addElement(...elements: Array<$el>) {
        for (var i : number = elements.length - 1; i >= 0; i--) {
            this.DOMELEMENT.append(elements[i]);
        }
    }
    public removeElement(...elements: Array<$el>) {
        for (var i : number = elements.length - 1; i >= 0; i--) {
            elements[i].remove();
        }
    }
}
