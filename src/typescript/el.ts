interface Document {
    attachEvent(event: string, listener: EventListener): boolean;
    detachEvent(event: string, listener: EventListener): void;
}
interface Window {
    XMLHttpRequest() : XMLHttpRequest;
}
interface HTMLElement {
    [key: string]: any;
    currentStyle: any;
}
interface CSSStyleDeclaration {
    [key: string]: any;
}
class $el extends Array {
    private static translateFunction: any = null;
	constructor(element: string|Array<HTMLElement>|HTMLElement|NodeList) {
        super();
		this.reload(element);
	}
	private reload(element: any): void {
		if (this.length > 0) {
            for (var i = 0; i < this.length; i++) {
                delete this[i];
            }
            this.length = 0;
        }
        if (typeof element === "string") {
            this[this.length] = document.createElement(element);
            this.length++;
        } else if (typeof element.length !== "undefined") {
            var elem = [].slice.call(element);
            for (var i = 0; i < elem.length; i++) {
                this[this.length] = elem[i];
                this.length++;
            }
        } else {
            this[this.length] = element;
            this.length++;
        }
	}
	public find(query: string): $el {
		return $el.find(query, this);
	}
	public static find(query: string, context?: any): $el {
		var list:Array<HTMLElement> = [];
		var $contexts:$el;
        if (typeof context === "undefined") {
            $contexts = new $el(document.documentElement);
        } else if (context instanceof $el) {
            $contexts = context;
        } else {
            $contexts = new $el(context);
        }
        $contexts.each(function() {
            if (query.match(/^\#([a-z0-9-_]+)$/i)) {
                list = list.concat([this.getElementById(query.substr(1))] as Array<HTMLElement>);
            } else if (query.match(/^\.([a-z0-9-_]+)$/i)) {
                list = list.concat([].slice.call(this.getElementsByClassName(query.substr(1))) as Array<HTMLElement>);
            } else if (query.match(/^([a-z0-9-_]+)$/i)) {
                list = list.concat([].slice.call(this.getElementsByTagName(query)) as Array<HTMLElement>);
            } else {
                list = list.concat([].slice.call(this.querySelectorAll(query)) as Array<HTMLElement>);
            }
        });
		return new $el(list);
	}
    public static fromHTML(html:string) {
        var elem = document.createElement('template');
        elem.innerHTML = html;
        return new $el(elem.childNodes);
    }
	public get(index: number): HTMLElement {
        return this[index];
    }
    public first(): HTMLElement{
        return this[0];
    }
    public last(): HTMLElement {
        return this[this.length - 1];
    }
    public addClass(className: string): $el {
        this.each(function() {
            if (typeof document.documentElement.classList !== "undefined") {
                this.classList.add(className);
            } else {
                if (!this._hasClass(className, this)) {
                    this.className += ' ' + className;
                }
            }
        });
        return this;
    }
    public removeClass(className: string): $el {
        this.each(function() {
            if (typeof document.documentElement.classList !== "undefined") {
                this.classList.remove(className);
            } else {
                if (!this._hasClass(className, this)) {
                    this.className = this.className.replace(new RegExp('\\b' + className + '\\b', 'g'), '');
                }
            }
        });
        return this;
    }
    public hasClass(className: string): boolean {
        return this._hasClass(className, this.first());
    }
    private _hasClass(className: string, element: HTMLElement): boolean {
        return typeof document.documentElement.classList !== "undefined" ?
			element.classList.contains(className) :
			new RegExp('\\b' + className + '\\b').test(element.className);
    }
    public matches(selector: string) {
        return this._matches(this.first(), selector);
    }
    private _matches(el: HTMLElement, selector:string) {
        var p: any = Element.prototype;
        var f: any = p.matches || p.matchesSelector || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || function(s:string) {
            return this._indexOf(el, document.querySelectorAll(s).length > 0 ? [].concat(document.querySelectorAll(s)) : []) !== -1;
        };
        return f.call(el, selector);
    }
    public parent(): $el{
        var parents: Array<HTMLElement> = [];
        this.each(function() {
            parents.push(this.parentNode);
        });
        return new $el(parents);
    }
    public parents(selector: string): $el {
        var parents: Array<HTMLElement> = [];
        var _this:$el = this;
        this.each(function() {
            var p = this;
            while (p.parentNode && p.parentNode != document.documentElement) {
                if (typeof selector !== "undefined") {
                    if (_this._matches(p.parentNode, selector)) {
                        parents.push(p.parentNode);
                    }
                } else {
                    parents.push(p.parentNode);
                }
                p = p.parentNode;
            }

        });
        return new $el(parents);
    }
    public children(selector: string): $el {
        var children:Array<HTMLElement> = [];
        var _this:$el = this;
        this.each(function() {
            if (this.hasChildNodes()) {
                if (typeof selector === "undefined") {
                    children = children.concat([].slice.call(this.childNodes));
                } else {
                    for (var i = 0; i < this.childNodes.length; i++) {
                        if (_this._matches(this.childNodes[i], selector)) {
                            children.push(this.childNodes[i]);
                        }
                    }
                }
            }
        });
        return new $el(children);
    }
    public getAttribute(attr:string):string {
        return this.first().getAttribute(attr);
    }
    public setAttribute(attr:string, val:string):$el {
        this.each(function() {
            this.setAttribute(attr, val);
        });
        return this;
    }
    public removeAttribute(attr:string): $el {
        this.each(function() {
            this.removeAttribute(attr);
        });
        return this;
    }
    public before(element: string | $el | HTMLElement) {
        if (typeof element === 'string') {
            this.each(function() {
                if ($el.translateFunction) {
                    element = (<string>element).replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                        var translated: string = $el.translateFunction(content);
                        if (translated === null) {
                            return '{t(' + content + ')}';
                        }
                        return translated;
                    });
                }
                this.insertAdjacentHTML('beforeBegin', element);
            });
        } else if (element instanceof $el) {
            this.each(function() {
                var _el:HTMLElement = this;
                var newList:Array<HTMLElement> = [];
                element.each(function() {
                    var child:Node = _el.parentNode.insertBefore(this, _el);
                    newList.push(child as HTMLElement);
                });
                element.reload(newList);
            });
        } else {
            this.each(function() {
                this.parentNode.insertBefore(element, this);
            });
        }
        return this;
    }
    public after(element: string | $el | HTMLElement):$el {
        var _this = this;
        if (typeof element === 'string') {
            if ($el.translateFunction) {
                element = (<string>element).replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                    var translated: string = $el.translateFunction(content);
                    if (translated === null) {
                        return '{t(' + content + ')}';
                    }
                    return translated;
                });
            }
            this.each(function() {
                this.insertAdjacentHTML('afterEnd', element);
            });
        } else if (element instanceof $el) {
            this.each(function() {
                var _el:HTMLElement = this;
                var newList:Array<HTMLElement> = [];
                element.each(function() {
                    if (_el.parentNode.lastChild == _el) {
                        var child:Node = _el.parentNode.appendChild(this);
                    } else {
                        var child:Node = _el.parentNode.insertBefore(this, _el.nextSibling);
                    }
                    newList.push(child as HTMLElement);
                });
                element.reload(newList);
            });
        } else {
            this.each(function() {
                if (this.parentNode.lastChild == this) {
                    this.parentNode.appendChild(element);
                } else {
                    this.parentNode.insertBefore(element, this.nextSibling);
                }
            });
        }
        return this;
    }
    public afterArray(element: Array<any>) {
        var _this = this;
        $el.each(element, function(i:any, e:any) {
            _this.after(e);
        });
        return this;
    }
    public append(element: string | $el | HTMLElement):$el {
        if (typeof element === 'string') {
            if ($el.translateFunction) {
                element = (<string>element).replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                    var translated: string = $el.translateFunction(content);
                    if (translated === null) {
                        return '{t(' + content + ')}';
                    }
                    return translated;
                });
            }
            this.each(function() {
                this.insertAdjacentHTML('beforeEnd', element);
            });
        } else if (element instanceof $el) {
            this.each(function() {
                var _el:HTMLElement = this;
                var newList:Array<HTMLElement> = [];
                element.each(function() {
                    var child:Node = _el.appendChild(this);
                    newList.push(child as HTMLElement);
                });
                element.reload(newList);
            });
        } else {
            this.each(function() {
                this.appendChild(element);
            });
        }
        return this;
    }
    public appendArray(element: Array<any>) {
        var _this = this;
        $el.each(element, function(i:any, e:any) {
            _this.append(e);
        });
        return this;
    }
    public prepend(element: string | $el | HTMLElement):$el {
        if (element) {
            if (typeof element === 'string') {
                if ($el.translateFunction) {
                    element = (<string>element).replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                        var translated: string = $el.translateFunction(content);
                        if (translated === null) {
                            return '{t(' + content + ')}';
                        }
                        return translated;
                    });
                }
                this.each(function() {
                    this.insertAdjacentHTML('afterBegin', element);
                });
            } else if (element instanceof $el) {
                this.each(function() {
                    var _el:HTMLElement = this;
                    var first:HTMLElement = this.childNodes[0];
                    var newList:Array<HTMLElement> = [];
                    element.each(function() {
                        var child:Node = _el.insertBefore(this, first);
                        newList.push(child as HTMLElement);
                    });
                    element.reload(newList);
                });
            } else {
                this.each(function() {
                    var first:HTMLElement = this.childNodes[0];
                    this.insertBefore(element, first);
                });
            }
        }
        return this;
    }
    public clone(withEvents:boolean = true, deep:boolean = true) {
        var newList:Array<HTMLElement> = [];
        this.each(function() {
            var node = this.cloneNode(deep);
            if (withEvents) {
                for (var event in this._events) {
                    if (this._events.hasOwnProperty(event)) {
                        var _event:Array<EventListener> = this._events[event];
                        for (var i:number = 0; i < _event.length; i++) {
                            if (typeof document.addEventListener !== "undefined") {
                                this.addEventListener(event, _event[i]);
                            } else {
                                this.attachEvent('on' + event, _event[i]);
                            }
                        }
                    }
                }
            }
            node._events = this._events;
            newList.push(node);
        });
        return new $el(newList);
    }
    public css(css:any, value?:string): $el|string {
        if (typeof css === "string" && typeof value === "undefined") {
            if (typeof getComputedStyle !== 'undefined') {
                return getComputedStyle(this.first(), null).getPropertyValue(css);
            } else {
                return this.first().currentStyle[css];
            }
        } else if (typeof css === "string" && typeof value !== "undefined") {
            this.each(function() {
                this.style[css] = value;
            });
        } else {
            this.each(function() {
                for (var k in css) {
                    if (css.hasOwnProperty(k)) {
                        this.style[k] = css[k];
                    }
                }
            });
        }
        return this;
    }
    public attr(attr:any, val?:string): $el|string {
        if (typeof val === "undefined") {
            if (typeof attr === "string") {
                return this.getAttribute(attr);
            } else {
                var _this: $el = this;
                $el.each(attr, function(key:string, val:string) {
                    if (typeof val === null) {
                        _this.removeAttribute(key);
                    } else {
                        if ($el.translateFunction) {
                            if (typeof val === 'string') {
                                val = val.replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                                    var translated: string = $el.translateFunction(content);
                                    if (translated === null) {
                                        return '{t(' + content + ')}';
                                    }
                                    return translated;
                                });
                            }
                        }
                        _this.setAttribute(key, val);
                    }
                });
            }
        } else {
            if (val === null) {
                this.removeAttribute(attr);
            }
            else {
                if ($el.translateFunction) {

                    if (typeof val === 'string') {
                        val = val.replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                            var translated: string = $el.translateFunction(content);
                            if (translated === null) {
                                return '{t(' + content + ')}';
                            }
                            return translated;
                        });
                    }
                }
                this.setAttribute(attr, val);
            }
        }
        return this;
    }
    public childNodes(value: Array<any>) {
        this.html('');
        for(var i = 0; i < value.length; i++) {
            this.appendArray(value[i]);
        }
        return this;
    }
    public html(html?:string) : $el | string {
        if (typeof html !== "undefined") {
            if ($el.translateFunction) {
                html = html.replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                    var translated: string = $el.translateFunction(content);
                    if (translated === null) {
                        return '{t(' + content + ')}';
                    }
                    return translated;
                });
            }
            this.each(function(){
                this.innerHTML = html;
            });
            return this;
        } else {
            return this.first().innerHTML;
        }
    }
    public val(val?: string): $el | string {
        if (typeof val !== "undefined") {
            this.each(function() {
                this.value = val;
            });
            return this;
        } else {
            return (<HTMLInputElement>this.first()).value;
        }
    }
    public text(val?: string): $el | string {
        if (typeof val !== "undefined") {
            this.each(function() {
                this.innerText = val;
            });
            return this;
        } else {
            return (this.first()).innerText;
        }
    }
    public remove() {
        this.revEach(function() {
            if (typeof this.parentNode !== "undefined") {
                this.parentNode.removeChild(this);
            }
        });
    }
    public outerHTML(html?:string): $el|string {
        if (typeof html !== "undefined") {
            if ($el.translateFunction) {
                html = html.replace(/{t\((.*?)\)}/g, function(match: any, content: any, offset: any, string: any) {
                    var translated: string = $el.translateFunction(content);
                    if (translated === null) {
                        return '{t(' + content + ')}';
                    }
                    return translated;
                });
            }
            this.each(function(){
                this.outerHTML = html;
            });
            return this;
        } else {
            return this.first().outerHTML;
        }
    }
    public get width() : number {
        return this.first().getBoundingClientRect().width;
    }
    public get height():number {
        return this.first().getBoundingClientRect().height;
    }
    public get innerWidth():number {
        return this.first().clientWidth;
    }
    public get innerHeight():number {
        return this.first().clientHeight;
    }
    public data(attr: any, val?: string): $el | string {
        if (typeof val === "undefined") {
            if (typeof attr === "string") {
                return this.getAttribute('data-' + attr);
            } else {
                var _this: $el = this;
                $el.each(attr, function(key:string, val:string) {
                    if (typeof val === null) {
                        _this.removeAttribute('data-' + key);
                    } else {
                        _this.setAttribute('data-' + key, val);
                    }
                });
            }
        } else {
            if (val === null) {
                this.removeAttribute('data-' + attr);
            }
            else {
                this.setAttribute('data-' + attr, val);
            }
        }
        return this;
    }
    public prop(prop: any, val?: any): any {
        if (typeof val === "undefined") {
            if (typeof prop === "string") {
                var first: HTMLElement = this.first();
                return first[prop];
            } else {
                var _this: $el = this;
                $el.each(prop, function(key:string, val:any) {
                    this.each(function() {
                        this[key] = val;
                    });
                });
            }
        } else {
            this.each(function() {
                this[prop] = val;
            });
        }
        return this;
    }
    public trigger(event:string):$el {
        this.each(function() {
            var _this = this;
            if (typeof this._events !== "undefined" && typeof this._events[event] !== "undefined") {
                for (var e = 0; e < this._events[event].length; e++) {
                    this._events[event][e].apply(_this, []);
                }
            }
        });
        return this;
    }
    public on(event:string, listener:EventListener|Array<EventListener>):$el {
        this.each(function() {
            if (typeof this._events === "undefined") {
                this._events = {};
            }
            if (typeof this._events[event] === "undefined") {
                this._events[event] = [];
            }
            if (typeof listener === "function") {
                if (typeof document.addEventListener !== "undefined") {
                    this.addEventListener(event, listener);
                } else {
                    this.attachEvent('on' + event, listener);
                }
                this._events[event].push(listener);
            }
            else {
                var _this = this;
                $el.each(listener, function(i: any, ev: EventListener) {
                    if (typeof document.addEventListener !== "undefined") {
                        _this.addEventListener(event, ev);
                    } else {
                        _this.attachEvent('on' + event, ev);
                    }
                    _this._events[event].push(ev);
                });
            }
        });
        return this;
    }
    public events(event:string):Array<any> {
        var _events : Array<any> = [];
        this.each(function() {
            if (this._events[event]) {
                $el.each(this._events[event], function(i: any, event: any) {
                    _events.push(event);
                });
            }
        });
        return _events;
    }
    public off(event:string, listener?:EventListener):$el {
        this.each(function() {
            if (typeof this._events !== "undefined" && typeof this._events[event] !== "undefined") {
                if (typeof listener !== "undefined") {
                    if (typeof document.removeEventListener !== "undefined") {
                        this.removeEventListener(event, listener);
                    } else {
                        this.detachEvent('on' + event, listener);
                    }
                    this._events[event].splice(this._events[event].indexOf(listener), 1);
                } else {
                    for (var i = 0; i < this._events[event].length; i++) {
                        var listener = this._events[event][i];
                        if (typeof document.removeEventListener !== "undefined") {
                            this.removeEventListener(event, listener);
                        } else {
                            this.detachEvent('on' + event, listener);
                        }
                    }
                    this._events[event] = [];
                }
            }
        });
        return this;
    }
	public static indexOf(needle:any, haystack:Array<any>): number {
		if (typeof haystack.indexOf !== "function") {
            for (var i = 0; i < haystack.length; i++) {
                if (haystack[i] === needle) {
                    return i;
                }
            }
            return -1;
        } else {
            return haystack.indexOf(needle)
        }
	}
    public each(func: any): $el {
        $el.each(this, func);
        return this;
    }
    public revEach(func: any): $el {
        $el.revEach(this, func);
        return this;
    }
	public static each(arr:any, func:any): void {
		if (arr.length) {
			for (var i:number = 0; i < arr.length; i++) {
				func.apply(arr[i], [i, arr[i]]);
			}
		} else {
			for (var name in arr) {
				if (arr.hasOwnProperty(name)) {
					func.apply(arr[name], [name, arr[name]]);
				}
			}
		}
	}
    public static revEach(arr:any, func:any): void {
        for (var i = arr.length - 1; i >= 0; i--) {
            func.apply(arr[i], [i, arr[i]]);
        }
    }
	public static from(item:any): $el {
		return new $el(item);
	}
    static extendsHTMLwithTranslate(translateFunction:any) {
        $el.translateFunction = translateFunction;
    }
    static ready(func:any) {
        if (document.readyState != 'loading') func();
        else if (document.addEventListener) document.addEventListener('DOMContentLoaded', func);
        else document.attachEvent('onreadystatechange', function() { if (document.readyState == 'complete') func(); });
    }
    static trim(string:string) {
            if (typeof string.trim !== "undefined") {
                return string.trim();
            } else {
                return string.replace(/^\s+|\s+$/g, '');
            }
        }
    static jsonParse(json:string) {
        if (typeof JSON !== "undefined") {
            return JSON.parse(json);
        } else {
            return eval(json);
        }
    }
    static get(url:string, func:any) {
        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        xhr.open('GET', url, false);
        xhr.onreadystatechange = function() {
            if (xhr.readyState > 3 && xhr.status == 200) func(xhr.responseText);
        };
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send();
        return xhr;
    }

}