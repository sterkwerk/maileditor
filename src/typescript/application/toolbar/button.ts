/// <reference path="../../el.ts" />
namespace Application {
    export namespace Toolbar {
        export class Button {
            public DOMELEMENT: $el;
            private _action: any;
            private _editor:any;
            constructor(editor?: Application.Editor, html?: string, action?: any) {
                this.DOMELEMENT = new $el('span');
                this.DOMELEMENT.html(html);
                this._action = action;
                this._editor = editor;
                this.DOMELEMENT.attr('draggable', 'false');
                this.DOMELEMENT.addClass('button');
            }
            html(html: string) {
                this.DOMELEMENT.html(html);
            }
            setAction(element: any, action: any) {
                var _this = this;
                if (this._action) {
                    if (this.DOMELEMENT.children('select').length) {
                        this.DOMELEMENT.children('select').off('change', this._action);
                    } else {
                        this.DOMELEMENT.off('mousedown', this._action);
                    }
                }
                this._action = function(e: any) {
                    action.apply(element, [this, e, _this._editor]);
                    if (e.stopPropagation) e.stopPropagation();
                    if (e.preventDefault) e.preventDefault();
                    e.cancelBubble = true;
                    e.returnValue = false;
                    return false;
                };
                if (this.DOMELEMENT.children('select').length) {
                    this.DOMELEMENT.children('select').on('change', this._action);
                } else {
                    this.DOMELEMENT.on('mousedown', this._action);
                }
            }
            getAction() {
                return this._action;
            }
        }
    }
} 