/// <reference path="../el.ts" />
/// <reference path="../arraytohtml.ts" />
namespace Application {
    export class Editor {
        public DOMELEMENT: $el;
        public SELECTED_DOMELEMENT: $el;
        private _watch: any = {};
        private _selfAttached: any = undefined;
        constructor() {
            this.DOMELEMENT = new $el('editor');
            this.SELECTED_DOMELEMENT = this.DOMELEMENT.clone();
            this.DOMELEMENT.childNodes(
                ArrayToHTML.parse([
                    'table', { cellspacing: 0, cellpadding: 0, border: 0, style: 'width: 600px'},
                    ['tbody', null,
                        ['tr', null,
                            ['td', {class: 'placeholder'},
                            'This is a placeholder.'
                            ]
                        ]
                    ]
                ])
            );
        }
        triggerWatch(_variable: string) {
            for (var nm in this._watch[_variable]) {
                if (this._watch[_variable].hasOwnProperty(nm)) {
                    this._watch[_variable][nm].apply(this, []);
                }
            }
        }
        joinWatch(_variable: string, nm: string, func:any) {
            if (typeof this._watch[_variable] === "undefined") {
                this._watch[_variable] = {};
            }
            this._watch[_variable][nm] = function() { func.apply(this, [this[_variable]]) };
        }
        reattachEvents() {
            var _this = this;
            var elements = this.DOMELEMENT.find('td.content');
            if (this._selfAttached) {
                elements.off('click', this._selfAttached);
                this.DOMELEMENT.off('click', this._selfAttached);
            }
            
            this._selfAttached = function(e:any) {
                _this.SELECTED_DOMELEMENT = $el.from(this);
                _this.triggerWatch('SELECTED_DOMELEMENT');
                e.stopPropagation();
            };
            
            elements.on('click', this._selfAttached);
            this.DOMELEMENT.on('click', this._selfAttached);
        }
        addElement(...elements: Array<$el>) {
            for (var i : number = elements.length - 1; i >= 0; i--) {
                this.DOMELEMENT.append(elements[i])
            }
        }
        removeElement(...elements: Array<$el>) {
            for (var i : number  = elements.length - 1; i >= 0; i--) {
                elements[i].remove();
            }
        }
    }
}