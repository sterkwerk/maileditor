/// <reference path="../el.ts" />
/// <reference path="editor.ts" />
/// <reference path="../arraytohtml.ts" />

namespace Application {
    export class Popup {
        public DOMELEMENT: $el;
        public static document: $el;
        constructor(html: Array<any>) {
            this.DOMELEMENT = new $el('popup');
            var popup = new $el('div').addClass('popup');
            var popupbg = new $el('div').addClass('popup-bg');
            popup.childNodes(ArrayToHTML.parse(html));
            this.DOMELEMENT.append(popupbg).append(popup);
            Application.Popup.document.append(this.DOMELEMENT);
        }
        close() {
            this.DOMELEMENT.remove();
        }
    }
}