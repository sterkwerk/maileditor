/// <reference path="../el.ts" />
/// <reference path="editor.ts" />
/// <reference path="../arraytohtml.ts" />

namespace Application {
    export class Dialog {
        public DOMELEMENT: $el;
        public static document: $el;
        constructor(html: Array<any>) {
            this.DOMELEMENT = new $el('dialogwindow');
            var dialog = new $el('div').addClass('dialog');
            var dialogbg = new $el('div').addClass('dialog-bg');
            dialog.childNodes(ArrayToHTML.parse(html));
            this.DOMELEMENT.append(dialogbg).append(dialog);
            Application.Dialog.document.append(this.DOMELEMENT);
        }
        close() {
            this.DOMELEMENT.remove();
        }
    }
}