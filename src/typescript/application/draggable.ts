/// <reference path="../el.ts" />
namespace Application {
    export class Draggable {
        protected DOMELEMENT: $el;
        private _hover: any;
        private hoverElements: $el;
        private _context: any;
        public static Context: any;
        constructor(view: $el | string, context?: HTMLElement, hover?: any) {
            var body: $el = $el.find('body');
            var _this: Draggable = this;
            this.DOMELEMENT = new $el('draggable');
            if (typeof view === "string") {
                this.DOMELEMENT.html(view);
            } else {
                this.DOMELEMENT.html((<string>((<$el>view).outerHTML())));
            }
            $el.find('draggable').remove();
            this.DOMELEMENT.css({ 'position': 'absolute', 'top': '-1000px', 'left': '-1000px' });
            this._hover = hover;
            body.on('mousemove', function(e: any) {
                _this.DOMELEMENT.css({ 'top': (e.clientY + 18) + 'px', 'left': (e.clientX + 18) + 'px' });
            });
            this._context = context;
            this.hoverElements = $el.find(hover.allowed.join(','));
            this.hoverElements.off('mouseleave').off('mouseenter').on('mouseleave', function() {
                $el.from(this).removeClass('hover');
            }).on('mouseenter', function() {
                $el.from(this).addClass('hover');
            });

            this._context.off('mouseup').on('mouseup', function() {
                $el.find('draggable').remove();
                _this._context.off('mouseup');
            });
            body.append(this.DOMELEMENT);
        }
        public set release(release: any) {
            var _this = this;
            this._context.off('mouseup').on('mouseup', function(e: any) {
               
                _this.hoverElements.removeClass('hover').off('mouseleave').off('mouseenter');
                _this._context.off('mouseup');
                $el.find('body').off('mousemove');
                release(e, _this.DOMELEMENT);
                _this.DOMELEMENT.remove();
            });
        };
        public remove() {
            this.hoverElements.removeClass('hover').off('mouseleave').off('mouseenter');
            this._context.off('mouseup');
            $el.find('body').off('mousemove');
            this.DOMELEMENT.remove();
        }
    }
}