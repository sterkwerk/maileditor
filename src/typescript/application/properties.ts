/// <reference path="../el.ts" />
/// <reference path="editor.ts" />
/// <reference path="../arraytohtml.ts" />

namespace Application {
    export class Properties {
        public DOMELEMENT: $el;
        constructor(editor: Application.Editor, properties: any) {
            this.DOMELEMENT = new $el('properties');
            this.DOMELEMENT.childNodes(this.propertiesToHTML(editor, properties));
          
            var _this = this;
            $el.each(properties, function(prop:any, property:any) {
                var $input = _this.DOMELEMENT.find('input[name="'+prop+'"]');
                property.initialize($input.get(0));
                editor.joinWatch('SELECTED_DOMELEMENT', 'bar-prop-' + prop , function() {
                    property.getValue(editor.SELECTED_DOMELEMENT, $input);
                });
                $input.on('focus', function() {
                   $el.from(document.body).trigger("click"); 
                });
                $input.on('change', function(){
                    property.setValue(editor.SELECTED_DOMELEMENT, $input);
                });
                $input.on('blur', function(){
                    property.setValue(editor.SELECTED_DOMELEMENT, $input);
                });
            });
        }

        addElement(element:any) {
            this.DOMELEMENT.find('.files').append(element.DOMELEMENT);
            return this;
        }
        
        propertiesToHTML(editor: Application.Editor,properties: Array<any>) {
            var html: Array<any> = [];
            for(var prop in properties) {
                if (properties.hasOwnProperty(prop)) {
                    var val: any = properties[prop];
                    var htmlString: Array<any> = ['li', {class: 'field'}, 
                        ['label', null, '{t(properties.'+prop+')}'],
                        ['input', {type: 'text', name: prop.toLowerCase(), value: val.default}]
                    ];
                    html.push(htmlString);
                }
            }
            return ArrayToHTML.parse(['ul', null, ...html]);
        }
    }
}