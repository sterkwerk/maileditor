namespace Application {
	export class Language {
		private static lang: any = {}
		public static language: string = '';
		static register(lang: string, langConf: any) {
			Language.lang = langConf;
			Language.language = lang;
		}
		static translate(word: string):string {
			if (typeof Language.lang[word] !== "undefined") {
				return Language.lang[word];
			} else if (word.indexOf('.') !== -1) {
				var wordList = word.split('.');
				var ln = Language.lang;
				
				for (var wordList_i: number = 0; wordList_i < wordList.length; wordList_i++) {
					var subWordList = wordList.slice(wordList_i, wordList.length);
					for (var subWordList_i = subWordList.length; subWordList_i > 0; subWordList_i--) {
						var subWord = subWordList.slice(0, subWordList_i).join('.');
						if (typeof ln[subWord] !== "undefined") {
							ln = ln[subWord];
							wordList_i += subWordList_i-1;
							break;
						}
					}
				}

				if (ln) {
					return ln;
				}
			}
			return null;
		}
	}
}