/// <reference path="../el.ts" />
/// <reference path="toolkit/seperator.ts" />
/// <reference path="toolkit/button.ts" />
/// <reference path="editor.ts" />
namespace Application {
	export class Toolkit {
		public DOMELEMENT: $el;
		constructor() {
			this.DOMELEMENT = new $el('toolkit');
		}
		loadConfig(editor: Editor, config : any) {
			var _this = this;
			$el.each(config, function(group_index:number, group: any) {
				if (group_index > 0) {
					var seperator: Toolkit.Seperator = new Toolkit.Seperator();
					_this.DOMELEMENT.append(seperator.DOMELEMENT);
				}
				$el.each(group, function(button_index:number, button:any) {
					var btn: Toolkit.Button = new Toolkit.Button();
					btn.setSources(button.icons[0]);
					btn.title = button.title;
					
					btn.setAction(button, button.action, editor);
					_this.DOMELEMENT.append(btn.DOMELEMENT);
				});
			});
		}
		addElement(...elements:Array<$el>) {
			for (var i : number = elements.length - 1; i >= 0; i--) {
				this.DOMELEMENT.append(elements[i]);
			}
		}
		removeElement(...elements: Array<$el>) {
			for (var i : number = elements.length - 1; i >= 0; i--) {
				elements[i].remove();
			}
		}
	}
}