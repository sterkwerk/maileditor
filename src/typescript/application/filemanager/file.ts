/// <reference path="../../el.ts" />
namespace Application {
    export namespace FileManager {
        export class File {
            public DOMELEMENT: $el;
            private _fileData: any;
            private _removeAction: Function = function() {}
            constructor(_rmAction:Function) {
                var _this = this;
                this._removeAction = _rmAction;
                this.DOMELEMENT = new $el('div');
                this.DOMELEMENT.addClass('file');
                this.DOMELEMENT.childNodes(
                    ArrayToHTML.parse(
                        ['div', {class: 'file-icon', 'data-filetype': 'new'}, 
                            ['div', null, 'new']
                        ],
                        ['div', {class: 'file-info'}, 
                            ['div', {dataName: 'name'}],
                            ['div', {dataName: 'size'}]
                        ],
                        ['div', {class: 'file-actions'},
                            '<i class="fa fa-trash-o fa-fw remove-file"></i>' 
                        ]
                    )
                );
                this.DOMELEMENT.find('.remove-file').on('mousedown', function(e) {
                    e.stopPropagation();
                });
                this.DOMELEMENT.find('.remove-file').on('click', function(e) {
                    var popup = new Application.Popup([
                        'div', {class: 'content'}, 
                            ['div', {class: 'title'}, '{t(remove.file.message)}'],
                            ['div', {class: 'buttons'}, 
                                ['span', {
                                    class: 'button bad',
                                    'data-name': 'remove',
                                    onclick: function() {
                                        _this.DOMELEMENT.remove();
                                        popup.close();
                                        _this._removeAction(_this._fileData);
                                    }
                                }, '{t(popup.remove)}'],
                                ['span', {class: 'button', onclick: function() {popup.close();}}, '{t(popup.cancel)}']
                            ]
                    ]);
                });
            }
            public setFileData(fileData:any, content:any):void {
                var ext = fileData.name.substr(fileData.name.lastIndexOf('.')+1).toLowerCase();
                this.DOMELEMENT.find('[data-name="name"]').html(fileData.name);
                this.DOMELEMENT.find('[data-name="size"]').html(this.formatFileSize(fileData.size));
                this.DOMELEMENT.find('.file-icon').attr('data-filetype', ext);
                this.DOMELEMENT.find('.file-icon > div').html(ext);
                fileData.content = content;
                fileData.filename = fileData.name;
                this._fileData = fileData;
            }
            public formatFileSize(filesize:number):string {
                if (filesize === 0) {
                    return '0 B';
                } else {
                    var index = Math.floor(Math.log(filesize) / Math.log(1024));
                    var fsz: number = filesize / Math.pow(1024, index);
                    return fsz.toFixed(2) + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][index];
                }
            }
            public getFileData() : any {
                return this._fileData;
            }
        }
    }
}