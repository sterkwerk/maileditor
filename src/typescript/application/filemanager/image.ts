/// <reference path="../../el.ts" />
/// <reference path="../draggable.ts" />
namespace Application {
    export namespace FileManager {
        export class Image {
            public DOMELEMENT: $el;
            private _fileData: any;
            private _removeAction: Function = function() {}
            public static resetDataSlot: Function = function() {}
            constructor(_rmAction:Function, fileData:any = null) {
                var _this = this;
                this._removeAction = _rmAction;
                this.DOMELEMENT = new $el('div');
                this.DOMELEMENT.addClass('file');
                this.DOMELEMENT.childNodes(
                    ArrayToHTML.parse(
                        ['div', {class: 'image'}, 
                            ['img', {dataName:'preview'}]
                        ],
                        ['div', {class: 'file-info'}, 
                            ['div', {dataName: 'name'}],
                            ['div', {dataName: 'size'}]
                        ],
                        ['div', {class: 'file-actions'},
                            '<i class="fa fa-trash-o fa-fw remove-file"></i>' 
                        ]
                    )
                );
                this.DOMELEMENT.find('.remove-file').on('mousedown', function(e) {
                    e.stopPropagation();
                });
                var moveable:Draggable = null;
                this.DOMELEMENT.find('.remove-file').on('click', function(e) {
                    if (moveable !== null) {
                        moveable.remove();
                    }
                    var popup = new Application.Popup([
                        'div', {class: 'content'}, 
                            ['div', {class: 'title'}, '{t(remove.file.message)}'],
                            ['div', {class: 'buttons'}, 
                                ['span', {
                                    class: 'button bad',
                                    'data-name': 'remove',
                                    onclick: function() {
                                        $el.find('editor').find('[data-slot="image"]').each(function(i:number, __this:any) {
                                            var $this = $el.from(__this);
                                            if ($this.find('img').matches('[data-src="'+_this._fileData.filename+'"]') || $this.find('img').matches('[data-src="'+_this._fileData.src+'"]')) {
                                                $this.attr('data-slot', null);
                                                $this.html('');
                                                Application.FileManager.Image.resetDataSlot($this);
                                            }
                                        });
                                        _this.DOMELEMENT.remove();
                                        popup.close();
                                        _this._removeAction(_this._fileData);
                                    }
                                }, '{t(popup.remove)}'],
                                ['span', {class: 'button', onclick: function() {popup.close();}}, '{t(popup.cancel)}']
                            ]
                    ]);
                });
                var hover = { allowed: ['.slot'] };
                this.DOMELEMENT.on('mousedown', function() {
                    var $img = $el.from(this).find('img').clone();
                    $img.attr('data-name', null);
                    $img.css('display', 'block');
                    var _img = $img;
                    moveable = new Application.Draggable($img, Application.Draggable.Context, hover);
                    moveable.release = function(e:any, $img:$el) {
                        var $target = $el.from(e.target);
                        if ($target.parents('.slot').length == 1) {
                            $target = $target.parents('.slot');
                        }
                        if (!$target.matches('.slot')) {
                            $target.off('click');
                            moveable = null;
                            return; 
                        }
                        var $remove = new $el('div');
                        $remove.addClass('remove');
                        var targetOldEvents = $target.events('click');
                        $remove.on('click', function(e) {
                            e.stopPropagation();
                            var p = $remove.parent().parent();
                            Application.FileManager.Image.resetDataSlot($remove.parent());
                            $remove.parent().on('click', targetOldEvents);
                            $remove.parent().attr('data-slot', null);
                            $remove.parent().html('');
                        });
                        if ($target.matches('.slot[data-slot="image"]')) {
                            $target.find('img').attr('src', _img.getAttribute('src'));
                            $target.find('img').attr('data-src', _this._fileData.filename);
                            $target.find('img').css('width', ($img.width < $target.innerWidth ? $img.width : $target.innerWidth) + 'px');
                            $target.find('img').attr('width', ($img.width < $target.innerWidth ? $img.width : $target.innerWidth) + 'px');
                            $target.find('img').css('height', 'auto');
                        } else if ($target.matches('.slot') && !$target.matches('[data-slot]')) {
                            var $image = new $el('img');
                            $image.attr('src', _img.getAttribute('src'));
                            $image.attr('data-src', _this._fileData.filename);
                            $image.css('width', ($img.width < $target.innerWidth ? $img.width : $target.innerWidth) + 'px');
                            $image.attr('width', (imgWidth < $target.innerWidth ? imgWidth : $target.innerWidth) + 'px');
                            $image.css('height', 'auto');
                            $image.css('display', 'block');
                            $target.data('slot', 'image');
                            $target.html('');
                            $target.append($image);
                            $target.append($remove);
                        } else if ($target.matches('.slot')) {
                            var imgWidth = $img.width;
                            var popit = new Application.Popup([
                                'div', {class: 'content'}, 
                                    ['div', {class: 'title'}, '{t(slot-set.'+$target.data('slot')+')}'],
                                    ['div', {class: 'buttons'}, 
                                        ['span', {
                                            class: 'button bad',
                                            'data-name': 'remove',
                                            onclick: function() {
                                                var $image = new $el('img');
                                                $image.attr('src', _img.getAttribute('src'));
                                                $image.attr('data-src', _this._fileData.filename);
                                                $image.css('width', (imgWidth < $target.innerWidth ? imgWidth : $target.innerWidth) + 'px');
                                                $image.css('height', 'auto');
                                                $image.css('display', 'block');
                                                $image.attr('width', (imgWidth < $target.innerWidth ? imgWidth : $target.innerWidth) + 'px')
                                                $target.data('slot', 'image');
                                                $target.html('');
                                                $target.append($image);
                                                $target.append($remove);
                                                popit.close();
                                            }
                                        }, '{t(popup.ok)}'],
                                        ['span', {class: 'button', onclick: function() {popit.close();}}, '{t(popup.cancel)}']
                                    ]
                            ]);
                        }
                        $target.off('click');
                        moveable = null;
                    };
                });
                this._fileData = fileData;
                if (fileData) {
                    this.setFileData(fileData);
                }
            }
            public setFileData(fileData:any, content?:any) {
                this.DOMELEMENT.find('[data-name="name"]').html(fileData.name);
                this.DOMELEMENT.find('[data-name="size"]').html(this.formatFileSize(fileData.size));
                this.DOMELEMENT.find('[data-name="preview"]').attr({ 'src': content, 'draggable': false, 'data-src': fileData.name });
                if (this.DOMELEMENT.find('[data-name="preview"]').width < this.DOMELEMENT.find('[data-name="preview"]').height) {
                    this.DOMELEMENT.find('[data-name="preview"]').addClass('portrait'); 
                }
                fileData.content = content;
                fileData.filename = fileData.name;
                this._fileData = fileData;
                return this;
            }
            private formatFileSize(filesize:number):string {
                if (filesize === 0) {
                    return '0 B';
                } else {
                    var index = Math.floor(Math.log(filesize) / Math.log(1024));
                    var fsz: number = filesize / Math.pow(1024, index);
                    return fsz.toFixed(2) + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][index];
                }
            }
            public getFileData() : any {
                return this._fileData;
            }
        }
    }
}