/// <reference path="../el.ts" />
/// <reference path="toolbar/seperator.ts" />
/// <reference path="toolbar/button.ts" />
/// <reference path="editor.ts" />
/// <reference path="language.ts" />
namespace Application {
    export class Toolbar {
        public DOMELEMENT: $el;
        constructor() {
            this.DOMELEMENT = new $el('toolbar');
        }
        loadConfig(editor: Editor, config: any) {
            var _this = this;
            $el.each(config, function(group_index:number, group:any) {
                if (group_index > 0) {
                    var seperator = new Toolbar.Seperator();
                    _this.DOMELEMENT.append(seperator.DOMELEMENT);
                }
                $el.each(group, function(button_index:number, button:any) {
                    var btn = new Toolbar.Button(editor);
                    var htm = button.html;
                    btn.html(htm);
                    if (button.load) {
                        button.load(btn.DOMELEMENT);
                    }
                    editor.joinWatch('SELECTED_DOMELEMENT', (<string>(group_index + '-' +button_index)), function() {
                        if (button.contentCheck) {
                            button.contentCheck(btn.DOMELEMENT, editor.SELECTED_DOMELEMENT);
                        }
                    });
                    btn.setAction(button, button.action);
                    _this.DOMELEMENT.append(btn.DOMELEMENT);
                });
            });
        }
        addElement(...elements:Array<$el>) {
            for (var i = elements.length - 1; i >= 0; i--) {
                this.DOMELEMENT.append(elements[i]);
            }
        }
        removeElement(...elements: Array<$el>) {
            for (var i = elements.length - 1; i >= 0; i--) {
                elements[i].remove();
            }
        }
    }
}