/// <reference path="../../el.ts" />
namespace Application {
    export namespace Toolkit {
        export class Button {
            public DOMELEMENT: $el;
            private _action: any;
            public editor: any;
            constructor() {
                this.DOMELEMENT = new $el('img');
                this.DOMELEMENT.attr('draggable', 'false');
            }
            public setSources(src:string) {
                this.DOMELEMENT.attr('src', src);
            }
            set title(title:string) {
                this.DOMELEMENT.prop('title', title);
            }
            get title() {
                return this.DOMELEMENT.prop('title');
            }
            setAction(element:HTMLElement, action:any, editor: any) {
                if (this._action) {
                    this.DOMELEMENT.off('mousedown', this._action);
                }
                this._action = function(e:any) {
                    action.apply(element, [this, e, editor]);
                };
                this.DOMELEMENT.on('mousedown', this._action);
            }
            getAction() {
                return this._action;
            }
        }
    }
}