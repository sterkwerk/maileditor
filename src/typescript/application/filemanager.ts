/// <reference path="../el.ts" />
/// <reference path="../arraytohtml.ts" />
/// <reference path="filemanager/image.ts" />
/// <reference path="filemanager/file.ts" />
namespace Application {
    export class FileManager {
        public DOMELEMENT: $el;
        private _files : Array<any> = [];
        private _onNewFile : Function = function() {
        
        };
        private _onRemFile : Function = function() {

        }
        constructor() {
            this.DOMELEMENT = new $el('filemanager');
            this.DOMELEMENT.childNodes(
                ArrayToHTML.parse(
                    ['section', {class: 'title'}, '{t(files)}'],
                    ['div', {class: 'files'}],
                    ['div', {class: 'input'},
                        ['input', {type: 'file', multiple: 'multiple'}],
                        ['div', {class: 'upload'},
                            ['i', {class: 'fa fa-cloud-upload fa-fw'}],
                            ' {t(upload.file)}'
                        ]
                    ]
                )
            );
            var _this = this;
            this.DOMELEMENT.find('.upload').on('click', function(e:any) {
                _this.DOMELEMENT.find('input').first().click();
            });
            this.DOMELEMENT.find('input').on('change', function(e:any) {
                $el.each(e.target.files, function(i:number, file:any) {
                    if (file.type.substr(0, 5) == 'image') {
                        var reader = new FileReader();

                        reader.addEventListener("load", function() {
                            var $file = new Application.FileManager.Image(function() { _this._onRemFile.apply(_this,arguments); });
                            $file.setFileData(file, reader.result);
                            _this._files.push($file.getFileData());
                            _this._onNewFile($file.getFileData());
                            _this.addElement($file);
                        }, false);

                        reader.readAsDataURL(file);
                    } else {
                        var reader = new FileReader();

                        reader.addEventListener("load", function() {
                            var $file = new Application.FileManager.File(function() { _this._onRemFile.apply(_this,arguments); });
                            $file.setFileData(file, reader.result);
                            _this._files.push($file.getFileData());
                            _this._onNewFile($file.getFileData());
                            _this.addElement($file);
                        }, false);

                        reader.readAsDataURL(file);
                    }
                });
                $el.from(this).val('');
            });
        }
        onNewFile(func: Function) {
            this._onNewFile = func;
        }
        onRemFile(func: Function) {
            this._onRemFile = func;
        }
        getFiles() {
            return this._files;
        }
        loadFiles(files:any ) {
            var _this = this;
            this.DOMELEMENT.find('.files').html('');
            for(var a = 0; a < files.length; a++) {
                var file = files[a];
                var $file:any = null;
                if (file.type.substr(0,5) === 'image') {
                    $file = new Application.FileManager.Image(function() { _this._onRemFile.apply(_this, arguments); });
                    $file.setFileData(file, file.src);
                    this._files.push(file.src);
                    this.addElement($file);
                } else {
                    $file = new Application.FileManager.File(function() { _this._onRemFile.apply(_this, arguments); });
                    $file.setFileData(file, file.src);
                    this._files.push($file.getFileData());
                    this.addElement($file);
                }
            }       
        }
        addElement(element:any) {
            this.DOMELEMENT.find('.files').append(element.DOMELEMENT);
            return this;
        }
    }
}